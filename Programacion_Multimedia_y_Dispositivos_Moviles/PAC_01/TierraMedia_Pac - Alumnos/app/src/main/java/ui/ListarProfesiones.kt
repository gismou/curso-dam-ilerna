package ui

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.TableRow
import android.widget.TextView
import bbdd.HabitantesSQLite
import com.elsda.tierramedia_pac.R

class ListarProfesiones : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_listar_profesiones)

        // Recuperamos el valor de la profesion pasado por el Intent
        val profesion = intent.getStringExtra("PROFESION")
        Log.d("ListarProfesionesActivity", "Profesion: $profesion")

        // Recuperamos el listado de habitantes por profesion de la Base de Datos
        val db = HabitantesSQLite(this)
        val listadoHabitantesProfesion = profesion?.let { db.getListadoPorProfesion(it) }

        // Asignamos los elementos de la UI
        val nombreProfesion = findViewById<TextView>(R.id.txtNombreProfesion)
        val numeroHabitantesProfesion = findViewById<TextView>(R.id.txtNumeroHabitantesProfesion)
        val btnAtras = findViewById<ImageButton>(R.id.btnAtrasListadoProfesion)
        val tableLayout = findViewById<LinearLayout>(R.id.layoutListadoProfesion)

        // Actualizamos los datos que necesitamos mostrar en el listado
        when (profesion) {
            "Alquimista" -> nombreProfesion.text = "Alquimistas"
            "Arquero" -> nombreProfesion.text = "Arqueros"
            "Caballero" -> nombreProfesion.text = "Caballeros"
            "Campesino" -> nombreProfesion.text = "Campesinos"
            "Carpintero" -> nombreProfesion.text = "Carpinteros"
            "Escriba" -> nombreProfesion.text = "Escribas"
            "Herrero" -> nombreProfesion.text = "Herreros"
            "Juglar" -> nombreProfesion.text = "Juglares"
            "Mercader" -> nombreProfesion.text = "Mercaderes"
            "Monje" -> nombreProfesion.text = "Monjes"
            else -> nombreProfesion.text = "Profesión desconocida: $profesion"
        }

        // Por cada habitante, crea una nueva TableRow y añádela al LinearLayout
        if (listadoHabitantesProfesion != null && listadoHabitantesProfesion.size > 0) {
            listadoHabitantesProfesion.forEach { habitante ->
                val fila = TableRow(this@ListarProfesiones)
                // Llama a insertarTextoEnTabla para cada detalle del habitante
                insertarTextoEnTabla(habitante.nombre, fila)
                insertarTextoEnTabla(habitante.apellidos, fila)
                insertarTextoEnTabla(habitante.raza, fila)
                insertarTextoEnTabla(habitante.ubicacion, fila)
                // Creamos un Button nuevo con la llamada a la funcion de la bd
                val btnBorrar = ImageButton(this@ListarProfesiones).apply {
                    setImageResource(android.R.drawable.ic_menu_close_clear_cancel) // Usar el ícono predeterminado de Android para la "X"
                    setBackgroundColor(Color.RED)
                    layoutParams = TableRow.LayoutParams(
                        0,
                        30,
                        1f
                    ).apply {
                        setMargins(0, 8, 0, 8) // Agregar un margen inferior para separarlo de la fila superior
                    }
                    setOnClickListener {
                        db.borrarHabitante(habitante.id)
                        // El método recreate refresca la vista actual
                        recreate()
                    }
                }

                fila.addView(btnBorrar)
                // Añade la fila al TableLayout después de añadir todos los TextViews
                tableLayout.addView(fila) // Asegúrate de tener una referencia a tu TableLayout
            }
        }

        // Añadimos el listener para volver a la Activity anterior
        btnAtras.setOnClickListener {
            // Cierra la Activity actual => cierra esta Activity y vuelva a la anterior en la pila
            finish()
        }

        // Ponemos todos los habitantes que hay de dicha raza en el textView
        val numeroHabitantes = db.getNumeroHabitantesPorProfesion(profesion ?: "")
        numeroHabitantesProfesion.text = numeroHabitantes.toString()
    }

    /**
     * Método para insertar texto en la tabla
     * @param texto cadena de texto a añadir en la fila
     * @param fila fila de la tabla donde añadiremos el texto
     */
    private fun insertarTextoEnTabla(texto: String, fila: TableRow) {
        val textView = TextView(this@ListarProfesiones).apply {
            text = texto
            // Ajusta estos layoutParams dependiendo de tu contenedor final
            layoutParams = TableRow.LayoutParams(
                0, // Usar 0 para el ancho en TableRow con layout_weight
                TableRow.LayoutParams.WRAP_CONTENT,
                1f // Peso para distribución equitativa en TableRow
            )
            textSize = 11f
            setBackgroundColor(Color.WHITE)
            setTextColor(Color.BLACK)
        }

        fila.addView(textView)
    }
}