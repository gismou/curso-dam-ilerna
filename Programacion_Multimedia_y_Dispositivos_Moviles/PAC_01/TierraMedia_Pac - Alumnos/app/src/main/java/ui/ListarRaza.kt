package ui

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import android.widget.LinearLayout
import android.widget.TableRow
import bbdd.HabitantesSQLite
import com.elsda.tierramedia_pac.R
import org.w3c.dom.Text

class ListarRaza : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_listar_raza)

        // Recuperamos el valor de la raza pasado por el Intent
        val raza = intent.getStringExtra("RAZA")
        Log.d("ListarRazaActivity", "Raza: $raza")

        // Recuperamos el listado de la Base de Datos
        val db = HabitantesSQLite(this)
        val listadoHabitantes = raza?.let { db.getListadoPorRaza(it) }
        // Asignamos los elementos de la UI
        val nombreRaza = findViewById<TextView>(R.id.txtNombreRaza)
        val numeroHabitantesRaza = findViewById<TextView>(R.id.txtNumeroHabitantesRaza)
        val btnAtras = findViewById<ImageButton>(R.id.btnAtrasListadoRaza)
        val tableLayout = findViewById<LinearLayout>(R.id.layoutListadoProfesion)

        // Actualizamos los datos que necesitamos mostrar en el listado
        when (raza) {
            "Elfo" -> nombreRaza.text = "Elfos"
            "Enano" -> nombreRaza.text = "Enanos"
            "Hombre" -> nombreRaza.text = "Hombres"
            "Hobbit" -> nombreRaza.text = "Hobbits"
            else -> nombreRaza.text = "Raza desconocida: $raza"
        }

        // Por cada habitante, crea una nueva TableRow y añádela al LinearLayout
        if (listadoHabitantes != null && listadoHabitantes.size > 0) {
            listadoHabitantes.forEach { habitante ->
                val fila = TableRow(this@ListarRaza)
                // Llama a insertarTextoEnTabla para cada detalle del habitante
                insertarTextoEnTabla(habitante.nombre, fila)
                insertarTextoEnTabla(habitante.apellidos, fila)
                insertarTextoEnTabla(habitante.edad.toString(), fila)
                insertarTextoEnTabla(habitante.ubicacion, fila)
                // Creamos un Button nuevo con la llamada a la funcion de la bd
                val btnBorrar = ImageButton(this@ListarRaza).apply {
                    setImageResource(android.R.drawable.ic_menu_close_clear_cancel) // Usar el ícono predeterminado de Android para la "X"
                    setBackgroundColor(Color.RED)
                    layoutParams = TableRow.LayoutParams(
                        0,
                        30,
                        1f
                    ).apply {
                          setMargins(0, 8, 0, 8) // Agregar un margen inferior para separarlo de la fila superior
                    }
                    setOnClickListener {
                        db.borrarHabitante(habitante.id)
                        // El método recreate refresca la vista actual
                        recreate()
                    }
                }

                fila.addView(btnBorrar)
                // Añade la fila al TableLayout después de añadir todos los TextViews
                tableLayout.addView(fila) // Asegúrate de tener una referencia a tu TableLayout
            }
        }

        // Añadimos el listener para volver a la Activity anterior
        btnAtras.setOnClickListener {
            // Cierra la Activity actual => cierra esta Activity y vuelva a la anterior en la pila
            finish()
        }

        // Ponemos todos los habitantes que hay de dicha raza en el textView
        val numeroHabitantes = db.getNumeroHabitantesPorRaza(raza ?: "")
        numeroHabitantesRaza.text = numeroHabitantes.toString()
    }

    /**
     * Método para insertar texto en la tabla
     * @param texto cadena de texto a añadir en la fila
     * @param fila fila de la tabla donde añadiremos el texto
     */
    private fun insertarTextoEnTabla(texto: String, fila: TableRow) {
        val textView = TextView(this@ListarRaza).apply {
            text = texto
            // Ajusta estos layoutParams dependiendo de tu contenedor final
            layoutParams = TableRow.LayoutParams(
                0, // Usar 0 para el ancho en TableRow con layout_weight
                TableRow.LayoutParams.WRAP_CONTENT,
                1f // Peso para distribución equitativa en TableRow
            )
            textSize = 11f
            setBackgroundColor(Color.WHITE)
            setTextColor(Color.BLACK)
        }

        fila.addView(textView)
    }

}