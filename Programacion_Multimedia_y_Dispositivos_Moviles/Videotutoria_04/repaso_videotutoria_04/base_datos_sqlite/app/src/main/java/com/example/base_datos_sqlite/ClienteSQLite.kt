package com.example.base_datos_sqlite

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class ClienteSQLite (context: Context): SQLiteOpenHelper(context,"Clientes.db",null,1) {

    override fun onCreate(db: SQLiteDatabase) {
        // Crear una tabla Clientes
        val createTableQuery = """
            CREATE TABLE clientes (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                nombre TEXT NOT NULL,
                dni TEXT NOT NULL
            )
        """.trimIndent()

        db.execSQL(createTableQuery)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        // Eliminar la tabla Clientes si existe y volver a crearla
        db.execSQL("DROP TABLE IF EXISTS clientes")
        onCreate(db)
    }

    /**
     * Insertar un cliente usando la clase Cliente
     * @param nuevoCliente los datos del nuevo coliente como objeto de la clase Cliente
     * @return el identificador del Cliente
     */
    fun insert(nuevoCliente: Cliente):Long{
        val db = getWritableDatabase()

        val values = ContentValues()
        values.put("nombre", nuevoCliente.nombre)
        values.put("dni",nuevoCliente.dni)

        val newId = db.insert("clientes",null,values)
        db.close()
        return newId
    }

    /**
     * Leer los datos de un cliente usando la clase Cliente
     * @param idCliente la ID del cliente en BBDD
     * @return el objeto Cliente con su información
     */
    fun read(idCliente:Long):Cliente{
        val db = getReadableDatabase()
        val selectQuery = "SELECT * FROM clientes WHERE id = ?"
        val cursor: Cursor = db.rawQuery(selectQuery,arrayOf(idCliente.toString()))
        var cliente = Cliente("","")
        if (cursor.moveToFirst()){
            val nombre = cursor.getString(cursor.getColumnIndexOrThrow("nombre"))
            val dni = cursor.getString(cursor.getColumnIndexOrThrow("dni"))
            cliente = Cliente(nombre,dni)
        }
        cursor.close()
        db.close()
        return cliente
    }

    /**
     * Actualizar un cliente con los datos de forma individual
     * @param idCliente ID del cliente en BBDD
     * @param cliente objeto de clase Cliente con los datos a actualizar
     * @return el numero de filas afectadas en un Int
     */
    fun update(idCliente:Long, cliente:Cliente):Int{
        val db = getWritableDatabase()

        val values = ContentValues()
        values.put("nombre",cliente.nombre)
        values.put("dni",cliente.dni)

        val affectedRows = db.update("clientes",values,"id = ?",arrayOf(idCliente.toString()))
        db.close()
        return affectedRows
    }

    /**
     * Método que elimina un cliente de la Base de Datos
     * @param idCliente ID del cliente en BBDD
     * @return el número de filas afectadas en un Int
     */
    fun delete(idCliente:Long):Int{
        val db = getWritableDatabase()
        val affectedRows = db.delete("clientes","id = ?",arrayOf(idCliente.toString()))
        db.close()
        return affectedRows
    }

    /**
     * Método para obtener el número de clientes de nuestra BBDD
     * @return el número de clientes en BBDD, si da error devuelve -1
     */
    fun getNumeroClientes(): Int{
        val db = getWritableDatabase()
        val selectQuery = "SELECT count(*) as numClientes FROM clientes"
        val cursor:Cursor = db.rawQuery(selectQuery,null)
        var num = -1
        if (cursor.moveToFirst()){
            num = cursor.getInt(cursor.getColumnIndexOrThrow("numClientes"))
        }
        cursor.close()
        db.close()
        return num
    }

    /**
     * Método para obtener el listado de todos los clientes
     * @return List<Cliente> una lista de clientes de la BBDD
     */
    fun getListadoClientes():List<Cliente>{
        val clientList = mutableListOf<Cliente>()
        val db = getWritableDatabase()
        val selectQuery = "SELECT * from clientes"
        val cursor:Cursor = db.rawQuery(selectQuery,null)

        if(cursor.moveToFirst()){
            do{
                val nombre = cursor.getString(cursor.getColumnIndexOrThrow("nombre"))
                val dni = cursor.getString(cursor.getColumnIndexOrThrow("id"))
                val cliente = Cliente(nombre,dni)
                clientList.add(cliente)
            }while(cursor.moveToNext())
        }
        cursor.close()
        db.close()
        return clientList
    }
}