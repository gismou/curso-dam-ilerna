package com.example.base_datos_sqlite

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Creamos los 3 clientes
        val c1 = Cliente("Mary James","11111111A")
        val c2 = Cliente("John Smith","22222222B")
        val c3 = Cliente("Anne Miller","33333333C")

        val bbddClientes = ClienteSQLite(this)
        bbddClientes.insert(c1)
        bbddClientes.insert(c2)
        bbddClientes.insert(c3)

        // Mostramos los clientes
        val listaClientes = bbddClientes.getListadoClientes()
        for(cliente in listaClientes){
            Log.i("MyActivity","Nombre: ${cliente.nombre} DNI: ${cliente.dni}")
        }
    }
}