package com.example.clientessqlite

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class MainActivity : AppCompatActivity() {

    // Para la base de datos Firebase -------------------------------------------------------
    fun insertarClientesFirebase(listadoClientes: List<Cliente>){
        // Guardamos el objeto FirebaseDatabase conectado con la instancia que previamente
        // hemos enlazado a la nube de firebase, posicionandonos en la raiz del árbol de la
        // base de datos no relacional.
        // getInstance te devuelve la raiz (la 'cima del arbol') de la base de datos no relacional
        val database: FirebaseDatabase = FirebaseDatabase.getInstance()
        // Desde la raiz del 'arbol' que es la base de datos no relacional, vamos a la
        // referencia (la 'tabla', el nodo en este caso) clientes.
        // getReference se ubica en dicho nodo, o lo crea si no existe
        val clientesRef = database.getReference("clientes")
        // guardamos los clientes que recibimos como listado
        listadoClientes.forEach {cliente->
            // Obtiene (CREO!) los pares clave-valor del nodo
            val key = clientesRef.push().key
            // De este modo insertamos los pares clave valor como hijos al nodo,  le pasamos
            // los datos del cliente que estamos recorriendo
            key?.let{
                clientesRef.child(it).setValue(cliente)
            }
        }
    }

    fun recuperarClientesFirebase(){
        val database: FirebaseDatabase = FirebaseDatabase.getInstance()
        val clientesRef = database.getReference("clientes")
        // El 'object' saldrá como error hasta que se implementan dos métodos (onDataChange y onCancelled)
        clientesRef.addListenerForSingleValueEvent(object: ValueEventListener {
            // Cada vez que se cambian los datos, se llama a este método
            override fun onDataChange(setClientes: DataSnapshot) {
                // Recorremos todos los nodos obtenidos en el Snapshot
                for (cliente in setClientes.getChildren()){
                    // de cada nodo general obtenemos los nodos hijos para obtener los
                    // 'campos' de la bbdd no relacional. El getValue con el String::class.java
                    // nos lo devuelve en String
                    val nombre = cliente.child("nombre").getValue(String::class.java)
                    val dni = cliente.child("dni").getValue(String::class.java)
                    // Lo imprimimos en logcat
                    Log.i("MyFirebase", "Nombre: ${nombre} DNI: ${dni}")
                }
            }

            override fun onCancelled(error: DatabaseError) {
                println("Error")
            }

        })
    }
    //---------------------------------------------------------------------------------------

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Creamos 3 clientes
        val c1 = Cliente("Pedro Perez","1111111A")
        val c2 = Cliente("Jon Smith","2222222B")
        val c3 = Cliente("Anne Johnson","33333333C")

        // Creamos el objeto bbdd (de clase ClientesSQlite, que hemos
        // declarado nosotros, pasandole el contexto, que es this.
        val bbddClientes = ClientesSQLite(this)
        bbddClientes.insert(c1)
        bbddClientes.insert(c2)
        bbddClientes.insert(c3)

        // Mostramos los clientes de la bbdd (los veremos en logcat)
        /*val listaClientes = bbddClientes.getListadoClientes()
        for(cliente in listaClientes){
            Log.i("MyActivity","Nombre: ${cliente.nombre} DNI: ${cliente.dni}")
        }*/

        // Creamos la lista con los objetos cliente creados
        val listadoClientesFirebase = listOf(c1,c2,c3);
        // Los insertamos en Firebase con la función que creamos
        // para ello previamente
        //insertarClientesFirebase(listadoClientesFirebase)

        // Mostramos los clientes recien creados
        recuperarClientesFirebase()
    }
}