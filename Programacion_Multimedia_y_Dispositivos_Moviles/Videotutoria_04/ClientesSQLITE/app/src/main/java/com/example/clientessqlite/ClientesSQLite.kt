package com.example.clientessqlite

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

/* El context es la Activity actual donde trabajara */
/* El SQLiteOpenHelper es la clase que heredamos para conectar con una BDD */
/* La clase de la que heredamos requiere que le pasemos el contexto donde va a actuar,
* el nombre de la base de datos, un conjunto de cursores (generalmente null) y la versión de la
* BDD que estamos trabajando */
class ClientesSQLite (context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    /* El companion object son atributos y métodos accesibles sin necesidad de
       instanciar la clase. (el static de java pero en kotlin)  */
    companion object {
        private const val DATABASE_NAME = "Clientes.db"
        private const val DATABASE_VERSION = 1;
    }

    /* La clase SQLiteHelper obliga a sobrescribir su onCreate, y el onUpgrade. onCreate se lanza al crear
    * una nueva base de datos y onUpgrade al actualizar la propia base de datos (cambiar a una versión mayor) */
    override fun onCreate(db: SQLiteDatabase){
        /* La triple comilla es para que el texto que vamos a poner entre ellas
           tenga en cuenta los saltos de linea */
        val createTableQuery = """
            /* Cuando creas un INTEGER en la base de datos, se crea como tipo Long */
            CREATE TABLE clientes (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                nombre TEXT NOT NULL,
                dni TEXT NOT NULL
            )
            """.trimIndent() // El trim para que quite espacios de principio y final, si los hubiera.

        db.execSQL(createTableQuery)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        // Elimino la tabla clientes, si existe, y la vuelvo a crear
        db.execSQL("DROP TABLE IF EXISTS clientes")
        onCreate(db)
    }

    // C R U D

    /**
     Insertar un cliente usando la clase Cliente
     *@param nuevoCliente los datos del nuevo cliente como objeto de la clase Cliente
     *@return el identificador del objeto Cliente
     */
    fun insert(nuevoCliente: Cliente): Long {
        /* El writableDatabase es una propiedad de la clase SQLiteOpenHelper
           que devuelve una instancia de SQLiteDataBase relacionada con la clase que
           la hereda y que le  permite realizar operaciones
           de escritura en la BD (si solo fuera de lectura sería readableDatabase */
        val db = writableDatabase

        /* ContentValues es una clase que proporciona android para guardar un conjunto
           de pares clave/valor, es decir una array asociativa */
        /* apply es una extension de kotlin para aplicar una serie de operaciones a un objeto
           y devolver el mismo objeto */
        /* esto seria lo mismo que haber creado el objeto vacio y luego aplicarle
           a este los put para ponerle valores, aunque de esta forma está en menos codigo*/
        /* Es decir es lo mismo que
            var values = ContentValues();
            values.put("nombre",nuevoCliente.nombre)
            values.put("dni",nuevoCliente.dni)
         */
        val values = ContentValues().apply {
            put("nombre",nuevoCliente.nombre) // A una clase de kotlin se le pueden meter nuevos atributos con put
            put("dni",nuevoCliente.dni)
        }

        /* El insert agrega un campo a la bd asociada, a la table que le indiquemos por parametro.
        * Necesita un null de segundo parametros (no sabemos porque)
        * y por ultimo el objeto ContentValues con los pares clave/valor de los campos de la
        * tabla. Nos devuelve el id que acaba de insertar */
        val numRowId = db.insert("clientes", null, values)

        /* Cerramos la base de datos después de una conexion */
        db.close()

        return numRowId;
    }

    /**
     * Actualiza el cliente con una determinada id
     * @param idCliente id del cliente en BBDD
     * @param cliente la instancia de Cliente con los datos a actualizar
     * @return el numero de filas afectadas
     */
    fun update(idCliente: Long, cliente: Cliente): Int{
        val db = writableDatabase

        val values = ContentValues().apply {
            put("nombre",cliente.nombre)
            put("dni",cliente.dni)
        }

        /* Para actualizar al bbdd usamos update, esta función necesita por parámetro el nombre
        * de la tabla a la que afectara, los valores que cambiara (en un ContentValues), la clausula
        * where que aplicará (las ? se irán sustituyendo en orden, como veremos ahora) y un array de
        * strings con los campos que vamos a sustituir en el la clausula where (es decir, las ?).
        * Para sustituirlas se hace en orden, es decir si tengo la clausula como "id = ? AND nombre = ?"
        * y le paso un array que es ("3","Pepe"), cambiará en orden de aparición las ? por el orden del array,
        * quedando en este ejemplo "WHERE id = "3" and nombre = "Pepe" */
        val affectedRows = db.update("clientes",values,"id = ?", arrayOf(idCliente.toString()))

        db.close();

        return affectedRows
    }

    /**
     * Leer los campos de la bdd de un cliente con un id concreto para convertirlo en objeto y
     * devolverlo.
     * @param idCliente el id del cliente a buscar
     * @return el cliente encontrado
     */
    fun read(idCliente: Long): Cliente{
        /* En lugar de la writabledatabase como en casos anteriores, como aqui solo leemos basta con readableDatabase */
        val db = readableDatabase;

        val selectQuery = "SELECT * FROM clientes WHERE id = "+idCliente.toString()

        /* Un Cursor es el resultado de una busqueda en bbdd guardada en un objeto */
        /* rawQuery permite lanzar una query cualquiera, pasandosela como primer parámetro, siendo el segundo los argumentos,
        * que de normal no pasaremos nada*/
        val cursor: Cursor = db.rawQuery(selectQuery, null)

        var cliente = Cliente("","");

        /* Desde la instancia de Cursor, intentamos mover al primer elemento con moveToFirst. Si no puede hacerlo devuelve false
        * y si puede true*/
        if(cursor.moveToFirst()){
            /* El getColumnIndex puede fallar al encontrar el nombre de la tabla que le indicamos, y eso daría un error, es
            *  por eso que al escribirlo sale en rojo. Para evitarlo podemos darle a la opción de corrección que nos propone,
            *  que hará que no de error por que le 'aseguramos' que va a encontrar si o si esa tabla en la bdd. Para indicar esto,
            *  añade la anotación @SuppressLint("Range") a el contexto (en este caso, antes de la función) */
            /* Otra opción para evitar el caso anterior (y la que haremos) es usar getColumnIndexOrThrow, que hace lo mismo pero
             * en caso de no encontrar el nombre del campo, lanza una excepción. */
            val nombre = cursor.getString(cursor.getColumnIndexOrThrow("nombre"))
            val dni = cursor.getString(cursor.getColumnIndexOrThrow("dni"))
            cliente = Cliente(nombre,dni);
        }

        cursor.close(); // El cursor tambien se cierra
        db.close();
        return cliente;
    }

    /**
     * Borra el cliente con el id indicado
     * @param idCliente el id del cliente a borrar
     * @return numero de filas afectadas
     */
    fun delete(idCliente: Long): Int{
        val db = writableDatabase
        /* delete funciona igual que el update, pero borrando */
        val affectedRows = db.delete("clientes","id=?",arrayOf(idCliente.toString()))
        db.close()
        return affectedRows
    }

    fun getNumeroClientes(): Int{
        val db = readableDatabase
        val selectQuery = "SELECT count(*) as numClientes FROM clientes"

        val cursor: Cursor = db.rawQuery(selectQuery,null)
        if(cursor.moveToFirst()){
            return cursor.getInt(cursor.getColumnIndexOrThrow("numClientes"))
        }

        cursor.close();
        db.close();

        return -1;
    }

    fun getListadoClientes(): Array<Cliente>{
        val db = readableDatabase
        var clientes: Array<Cliente> = arrayOf();
        val selectQuery = "SELECT * FROM clientes"
        val cursor: Cursor = db.rawQuery(selectQuery,null)
        if(cursor.moveToFirst()){
            do{
                val nombre = cursor.getString(cursor.getColumnIndexOrThrow("nombre"))
                val dni = cursor.getString(cursor.getColumnIndexOrThrow("dni"))
                val c:Cliente = Cliente(nombre,dni);
                clientes += c
            }while(cursor.moveToNext())
        }
        return clientes;
    }
}