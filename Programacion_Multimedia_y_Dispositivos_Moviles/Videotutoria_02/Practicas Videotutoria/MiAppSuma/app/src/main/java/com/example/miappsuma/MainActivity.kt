package com.example.miappsuma

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val button: Button = findViewById(R.id.btnSumar)
        val opA: TextView = findViewById(R.id.txtOperandoA)
        val opB: TextView = findViewById(R.id.txtOperandoB)
        val res: TextView = findViewById(R.id.txtResultado)

        button.setOnClickListener{
            val numA: Int = Integer.parseInt(opA.toString())
            val numB: Int = Integer.parseInt(opB.toString())
            res.text = (numA+numB).toString()
        }
    }
}