/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.pruebaunitaria;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author gismo
 */
public class PruebaUnitariaTest {
    
    public PruebaUnitariaTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of numero_mayor method, of class PruebaUnitaria.
     */
    @Test
    public void testNumero_mayor() {
        System.out.println("numero_mayor");
        int a = 0;
        int b = 0;
        int c = 0;
        PruebaUnitaria instance = new PruebaUnitaria();
        int expResult = 0;
        int result = instance.numero_mayor(a, b, c);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The result was "+result);
    }
    
     /**
     * Test of numero_mayor method, of class PruebaUnitaria.
     */
    @Test
    public void testNumero_mayor2() {
        System.out.println("numero_mayor");
        int a = 120;
        int b = -30;
        int c = 10;
        PruebaUnitaria instance = new PruebaUnitaria();
        int expResult = 120;
        int result = instance.numero_mayor(a, b, c);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The result was "+result);
    }
    
     /**
     * Test of numero_mayor method, of class PruebaUnitaria.
     */
    @Test
    public void testNumero_mayor3() {
        System.out.println("numero_mayor");
        int a = -10;
        int b = 0;
        int c = -30;
        PruebaUnitaria instance = new PruebaUnitaria();
        int expResult = 0;
        int result = instance.numero_mayor(a, b, c);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The result was "+result);
    }
    
     /**
     * Test of numero_mayor method, of class PruebaUnitaria.
     */
    @Test
    public void testNumero_mayor4() {
        System.out.println("numero_mayor");
        int a = 10;
        int b = 10;
        int c = 1110;
        PruebaUnitaria instance = new PruebaUnitaria();
        int expResult = 1110;
        int result = instance.numero_mayor(a, b, c);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The result was "+result);
    }
    
}
