/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.pruebaunitaria;

/**
 *
 * @author gismo
 */
public class PruebaUnitaria {
    
    public int numero_mayor(int a, int b, int c){
        // All numbers equals, we return one of them, for example a
        if(a == b && a == c){
            return a;
        }
        
        // If numbers aren't equals, we return the bigger number.
        if(a>b && a>c){
            return a;
        } else if (c > b){
            return c;
        } else {
            return b;
        }
    
    }
}
