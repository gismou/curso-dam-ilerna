package hundirflota;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.border.*;
import hundirflota.colorBarcos.*;

/**
 * Clase T�tulo
 *
 * @author ilernagames
 */
public class Titulo extends JPanel {

    JPanel barra;
    JPanel panel_titulo = new JPanel();
    JLabel titulo;
    Ventana ventana;

    /**
     * Constructor de la clase T�tulo
     */
    public Titulo() {
        barra = new JPanel();
        setLayout(new GridLayout(2, 1));
        panel_titulo.setBorder(new EmptyBorder(0, 0, 0, 0));
        add(barra);
        add(panel_titulo);
        this.setBorder(new EmptyBorder(0, 0, 0, 0));
        // Datos del t�tulo
        titulo = new JLabel("ILERNAGAMES - HUNDIR LA FLOTA", SwingConstants.CENTER);
        titulo.setFont(new Font("Verdana", 1, 17));
        titulo.setForeground(Color.DARK_GRAY);
        titulo.setVisible(true);
        crearMenu();
        crearMenuTamanyo();
        panel_titulo.add(titulo);
    }
    
    private void crearMenuTamanyo(){
        JMenuBar menuModo = new JMenuBar();
        JMenu modo_visibilidad = new JMenu("Tama�o Ventana");
        JMenuItem peque = new JMenuItem("Peque�o");
        peque.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ventana.setSize(new Dimension(300, 500));
            }
        });
        JMenuItem medio = new JMenuItem("Mediano");
        medio.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ventana.setSize(new Dimension(500, 700));
            }
        });
        JMenuItem grande = new JMenuItem("Grande");
        grande.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ventana.setSize(new Dimension(700, 900));
            }
        });
        modo_visibilidad.add(peque);
        modo_visibilidad.add(medio);
        modo_visibilidad.add(grande);
        menuModo.add(modo_visibilidad);
        barra.add(menuModo);
    }
    
    /**
     * Crea el men� para elegir los colores
     * AQU� CREAREMOS EL MEN� DE LOS COLORES
     */
    private void crearMenu() {
        JMenuBar menu = new JMenuBar();
        JMenu color_barcos = new JMenu("Color Barcos");
        // Color Marr�n
        JMenuItem color_marron = new JMenuItem("Marr�n");
        color_marron.addActionListener(new EColorMarron());
        // Color Morado
        JMenuItem color_morado = new JMenuItem("Morado");
        color_morado.addActionListener(new EColorMorado());
        // Color Naranja
        JMenuItem color_naranja = new JMenuItem("Naranja");
        color_naranja.addActionListener(new EColorNaranja());
        // Color Rosa
        JMenuItem color_rosa = new JMenuItem("Rosa");
        color_rosa.addActionListener(new EColorRosa());
        // Color Verde
        JMenuItem color_verde = new JMenuItem("Verde");
        color_verde.addActionListener(new EColorVerde());
        // Color Rojo
        JMenuItem color_rojo = new JMenuItem("Rojo");
        color_rojo.addActionListener(new EColorRojo());
        // Color Morado
        JMenuItem color_amarillo = new JMenuItem("Amarillo");
        color_amarillo.addActionListener(new EColorAmarillo());
        // Color Azul
        JMenuItem color_azul = new JMenuItem("Azul");
        color_azul.addActionListener(new EColorAzul());
        // A�adimos los diferentes colores
        color_barcos.add(color_marron);
        color_barcos.add(color_morado);
        color_barcos.add(color_naranja);
        color_barcos.add(color_rosa);
        color_barcos.add(color_verde);
        color_barcos.add(color_rojo);
        color_barcos.add(color_amarillo);
        color_barcos.add(color_azul);
        menu.add(color_barcos);
        barra.add(menu);
    }

    class eColorVerde implements ActionListener {

        Color color = Color.green;

        @Override
        public void actionPerformed(ActionEvent e) {

            for (int x = 0; x < 100; x++) {
                if (Partida.tableroJugador.botones[x].getActivo()
                        && !Partida.tableroJugador.botones[x].getTocado()
                        && !Partida.tableroJugador.botones[x].getHundido()) {
                    Partida.tableroJugador.botones[x].setBackground(color);
                }
            }
            Boton.color = color;
        }

    }
    
    public void setVentana(Ventana ventana) {
        this.ventana = ventana;
    }
}