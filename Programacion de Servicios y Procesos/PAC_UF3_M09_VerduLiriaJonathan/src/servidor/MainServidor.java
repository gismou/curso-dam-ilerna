package servidor;

import java.io.IOException;

public class MainServidor {

	public static void main(String[] args) throws IOException {
		
		// Creamos el objeto Servidor
		Servidor s = new Servidor();
		
		// Mensaje de inicio de servidor
		System.out.println("Servidor Arrancado \n");
		
		// Iniciamos el servidor
		s.start();
		
	}

}
