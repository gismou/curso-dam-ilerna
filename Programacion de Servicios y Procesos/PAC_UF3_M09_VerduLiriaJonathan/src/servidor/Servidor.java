package servidor;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor {

	// Como atributos un servidor necesitará el puerto, que será una constante, así como el ServerSocket y el Socker que inicializaremos.
	// Inicializaremos vacio un ServicioPass, que es la clase que tiene las funciones para crear la contraseña en base a un objeto de 
	// RequisitosPass, que es la clase que contiene los requisitos de dicha contraseña.
	private final int PORT = 4321;
	private ServerSocket serverSocket = new ServerSocket(PORT);
	private Socket socket = new Socket();
	private ServicioPass servicioPass = new ServicioPass();
	
	// Tendremos un constructor vacio, que lance la IOException por si se produce.
	public Servidor() throws IOException{};
	
	/**
	 * Método para iniciar el servidor, esperar las conexiones con clientes y una vez producidas lanzar el flujo de preguntas y respuestas con este
	 * con el fin de generar al final (o no) una contraseña.
	 * @throws IOException
	 */
	public void start() throws IOException {
		// Hacemos un bucle infinito, de este modo mantenemos el servidor abierto continuadamente a la espera de recibir clientes.
		while(true) {
			
			// -- CONECTAR CON EL CLIENTE ---------------------------------------------------------------------------------------------------
			
			System.out.println("Esperando al cliente");
			
			// Asignamos como socket del objeto Servidor la conexión entrante. El método accept está a la espera de recibir dicha conexión
			// para devolver de esta un objeto socket, que representa la conexión.
			this.socket = this.serverSocket.accept();
			
			System.out.println("Cliente conectado desde "+socket.getInetAddress());
			
			// Con un objeto DataOutputStream creamos un flujo de salida que conectará con el outputStream del socket. De este modo podremos
			// enviar mensajes al cliente desde el servidor.
			DataOutputStream salidaAlCliente = new DataOutputStream(this.socket.getOutputStream());
	        
	        // Creamo con el objeto DataInputStream un flujo de entrada, conectando con el flujo de entrada del socket, pudiendo así recibir
	        // los mensajes del cliente hasta aquí, el servidor.
	        DataInputStream entradaDelCliente = new DataInputStream(this.socket.getInputStream());
	        
	        // -- FLUJO DE MENSAJES CON EL CLIENTE -------------------------------------------------------------------------------------------
	        
	        // Le mandamos al cliente un mensaje presentandonos como el servidor y pidiendole su nombre.
	        salidaAlCliente.writeUTF("Hola, soy un servidor. ¿Como te llamas?");    
	        
	        // Esperamos la respuesta del cliente y en recibirla guardamos la respuesta como su nombre
	        String nombreCliente = entradaDelCliente.readUTF();
	        
	        // Mostramos por pantalla en el servidor su nombre
	        System.out.println("Nombre del cliente: "+nombreCliente);
	        
	        // Le mandamos un saludo de bienvenida personalizado con su nombre
	        salidaAlCliente.writeUTF("Te doy la bienvenida "+nombreCliente);
	       
	        // Le mandamos el aviso de como vamos a proceder para generar la contraseña y le hacemos la primera pregunta
	        // sobre las minusculas
	        salidaAlCliente.writeUTF("Voy a solicitar distintos requisitos para la contraseña que voy a generar.\nCuantas minusculas debe tener la contraseña?");
	       
	        // Recogemos la respuesta del usuario sobre las minusculas
	        int numMinusculas = entradaDelCliente.read();
	        
	        // Preguntamos sobre las mayusculas de la contraseña y recibimos la respuesta
	        salidaAlCliente.writeUTF("Cuantas mayusculas debe tener la contraseña?");
	        int numMayusculas = entradaDelCliente.read();
	        
	        // Preguntamos sobre los digitos de la contraseña y recibimos la respuesta
	        salidaAlCliente.writeUTF("Cuantos digitos debe tener la contraseña?");
	        int numDigitos = entradaDelCliente.read();
	        
	        // Preguntamos sobre los caracteres especiales de la contraseña y recibimos la respuesta
	        salidaAlCliente.writeUTF("Cuantos caracteres especiales debe tener la contraseña?");
	        int numEspeciales = entradaDelCliente.read();
	        
	        // Una vez tenemos todos los datos correctos para la contraseña creamos un objeto RequisitosPass, que es el objeto que tiene los datos
	        // de los requisitos para la contraseña. Una vez tenemos el objeto se lo asicamos al objeto del servidor de ServicioPass.
	        RequisitosPass requisitosPass = new RequisitosPass(numEspeciales,numDigitos,numMayusculas,numMinusculas);
	        servicioPass.setRequisitosPass(requisitosPass);
	        
	        // Mensaje en el servidor mostrando el toString del objeto RequisitosPass
	        System.out.println("Los requisitos del cliente son los siguientes\n"+requisitosPass.toString());
	        
	        // Obtenemos el total de caracteres que tendrá la contraseña, para ello usamos uan función de servicioPass que nos da dicha información.
	        int numTotal = servicioPass.longitudPass();
	        
	        // Mandamos un mensaje indicando de cuantos carácteres será la contraseña y la pregunta de si quiere generar la contraseña, obteniendo la 
	        // respuesta del cliente. 
	        salidaAlCliente.writeUTF("La longitud de la contraseña que se va a generar es de "+numTotal+" caracteres.\nQuieres generar una contraseña ahora? [si/no]");
	        System.out.println("Se ha enviado la longitud de la contraseña al cliente");
	        String respuesta = entradaDelCliente.readUTF();
	        
	        
	        // Según si ha dicho de generar o no la contraseña hacemos una cosa u otra
	        if(respuesta.equals("si")) {
	        	// Si se dice que si, se generará la contraseña y se mandará al usuario
	        	String pass = servicioPass.generaPass();
	        	salidaAlCliente.writeUTF("La contraseña generada es: "+pass);
	        	System.out.println("Se ha enviado la contraseña al cliente");
	        }else {
	        	// Si se dice que no, se manda un mensaje de despedida
	        	salidaAlCliente.writeUTF("No se generara ninguna contraseña. Hasta la proxima.");
	        	System.out.println("El cliente no desea generar una contraseña");
	        }
		}
	}
	
	public ServerSocket getServerSocket() {
		return serverSocket;
	}

	public Socket getSocket() {
		return socket;
	}

	public int getPORT() {
		return PORT;
	}
	
}
