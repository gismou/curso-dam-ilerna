package servidor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class ServicioPass {
	
	// Objeto RequisitosPass que contiene los requisitos de caracteres de cada tipo que se requieren para la contraseña generada
	private RequisitosPass requisitosPass;
	
	// Arrays finales con los caracteres que pertenecen a las diferentes categorias pedidas para la contraseña
    private static final String NUMEROS = "0123456789";
	private static final String MINUSCULAS = "abcdefghijklmnopqrstuvwxyz";
    private static final String MAYUSCULAS = MINUSCULAS.toUpperCase();
    private static final String ESPECIALES = "!@#$%^&*()_-+=.:?";
	
	/**
	 * Genera una contraseña usando como condiciones los atributos indicados en RequisitosPass
	 * @return la contraseña con el formato esperado
	 */
	public String generaPass() {
		String pass = "";
		
		// Usaremos la clase Random para con ella acceder a posiciones aleatorias de cada array de grupo de caracteres concretos de la contraseña.
		// El uso de la clase Random lo he sacado de internet, de esta página --> https://codegym.cc/es/groups/posts/es.825.clase-java-util-random
		Random random = new Random();
        
		// Primero generaremos el password en bloque, es decir primero los digitos, luego los especiales, luego las mayusculas y por ultimo
		// las minusuclas, generando cada bloque de forma aleatoria obteniendo al azar una posición de su array correspondiente.

        for (int i = 0; i < requisitosPass.getNumDigitos(); i++) {
        	pass += NUMEROS.charAt(random.nextInt(NUMEROS.length()));
        }
        
        for (int i = 0; i < requisitosPass.getNumCaractEspeciales(); i++) {
        	pass += ESPECIALES.charAt(random.nextInt(ESPECIALES.length()));
        }
        
        for (int i = 0; i < requisitosPass.getNumMayusculas(); i++) {
        	pass += MAYUSCULAS.charAt(random.nextInt(MAYUSCULAS.length()));
        }
        
        for (int i = 0; i < requisitosPass.getNumMinusculas(); i++) {
        	pass += MINUSCULAS.charAt(random.nextInt(MINUSCULAS.length()));
        }
        
        // Por seguridad queremos que quede todo mezclado, asi que ahora mezclaremos los caracteres del String del password generado por bloques de antes.
        return mezclarCadena(pass);
	}
	
	/**
	 * Método para mezclar una cadena de texto, convitiendola en un ArrayList para usar un metodo de las mismas que permite mezclar las posiciones
	 * para luego una vez mezclada volver a convertirla en String.
	 * Este método lo saqué de internet, de esta página --> https://www.geeksforgeeks.org/how-to-shuffle-characters-in-a-string-in-java/
	 * @param cadena la cadena que queremos mezclar
	 * @return la cadena mezclada
	 */
	public static String mezclarCadena(String cadena) { 
        // Convertimos la cadena a una Lista, para ello la recorremos entera (un String es un array de chars) con un foreach y la vamos agregando
		// al ArrayList.
        List<Character> caracteres = new ArrayList<>(); 
        for (char c : cadena.toCharArray()) { 
        	caracteres.add(c); 
        } 
  
        // La clase Collections (padre de todos los tipos de List, por ende usable sus metodos en un ArrayList) tiene un método para mezclar la colección.
        Collections.shuffle(caracteres); 
  
        // Volvemos a pasar el arrayList a un String, que es el qeu devolveremos. Para ello debemos hacer uso de un objeto StringBuilder
        // al que iremos añadiendo el arrayList ya mezclado, que recorremos con foreach.
        StringBuilder cadenaMezclada = new StringBuilder(); 
        for (char c : caracteres) { 
        	cadenaMezclada.append(c); 
        } 
  
        // Devolvemos el StringBuilder, pero pasandolo a String.
        return cadenaMezclada.toString(); 
    } 
	
	/**
	 * Suma todos los caracteres que tendrá a contraseña
	 * @return  el total de caracteres que tendrá la contraseña
	 */
	public int longitudPass() {
		return requisitosPass.getNumCaractEspeciales() + requisitosPass.getNumDigitos() + 
			   requisitosPass.getNumMayusculas() + requisitosPass.getNumMinusculas();
	}

	public void setRequisitosPass(RequisitosPass requisitosPass) {
		this.requisitosPass = requisitosPass;
	}
	
}
