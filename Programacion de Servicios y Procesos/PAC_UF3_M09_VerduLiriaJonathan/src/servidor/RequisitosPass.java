package servidor;

import java.util.InputMismatchException;
import java.util.Scanner;

public class RequisitosPass {
	int numCaractEspeciales;
	int numDigitos;
	int numMayusculas;
	int numMinusculas;
	
	public RequisitosPass(int numCaractEspeciales, int numDigitos, int numMayusculas, int numMinusculas) {
		this.numCaractEspeciales = numCaractEspeciales;
		this.numDigitos = numDigitos;
		this.numMayusculas = numMayusculas;
		this.numMinusculas = numMinusculas;
	}
	
	/**
	 * Funcion para asegurar que el numero que pedimos sea un numero mayor de 0 y no dejar que se siga hasta que
	 * lo sea.
	 * @param sc el objeto Scanner que usamos para leer
	 * @return el numero ya en el formato correcto, si no fuera en ese formato seguiría en bucle infinito
	 */
	public static int pideNum(Scanner sc) {
	    int numero = -1;
	    do {
	        try {
	            numero = sc.nextInt();
	            if(numero <= 0) System.err.println("Error: Debes ingresar un numero y este debe ser mayor de 0, intentalo de nuevo.");
	        } catch (InputMismatchException e) {
	            // Mostramos el mensaje de error en caso de que no se ingresa un número válido
	            System.err.println("Error: Debes ingresar un numero y este debe ser mayor de 0, intentalo de nuevo.");
	            // Limpiamos el búfer del scanner
	            sc.nextLine();
	        }
	    } while (numero <= 0);
	    
	    return numero;
	}
	
	/**
	 * Funcion para comprobar que lo que se le pasa sea 'si' o 'no'.
	 * @param sc el objeto Scanner que usamos para leer
	 * @return el resultado como se espera, si no fuera en ese formato seguiría en bucle infinito
	 */
	public static String siOno(Scanner sc) {
	  String respuesta = "";
	    do { 
	        // Pasamos la respuesta a minúsculas
	        respuesta = sc.nextLine().trim().toLowerCase();
	        
	        // Verificamos si la respuesta es diferente de 'si' y 'no'
	        if (!respuesta.equals("si") && !respuesta.equals("no")) {
	            // Si la respuesta no es válida, mostramos un mensaje de error
	            System.err.println("Error: Debes ingresar 'si' o 'no', intentalo de nuevo.");
	        }
	        
	    } while (!respuesta.equals("si") && !respuesta.equals("no"));
	    
	    return respuesta;
	}

	public int getNumCaractEspeciales() {
		return numCaractEspeciales;
	}

	public int getNumDigitos() {
		return numDigitos;
	}

	public int getNumMayusculas() {
		return numMayusculas;
	}

	public int getNumMinusculas() {
		return numMinusculas;
	}

	@Override
	public String toString() {
		return "PasswordReqs{minusculas=" + numMinusculas + ", mayusculas=" + numMayusculas
				+ ", digitos=" + numDigitos + ", caracteresEspeciales=" + numCaractEspeciales + "}";
	}
	
}
