package cliente;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

import servidor.RequisitosPass;

public class Cliente {

	// Como atributos un cliente necesitará un host y un port (serán constantes, es decir final) con los que poder luego realizar la conexión al socket, guardando
	// una instancia de Socket como atributo de la clase al llamar al constructor.
	private final String HOST = "localhost";
	private final int PORT = 4321;
	private Socket socket;
	
	// Tenemos un constructor donde creamos el socket. Dado que crear la instancia de socket puede dar una IOException, indicamos que 
	// el constructor puede lanzar dicha excepción (con el throws IOException).
	public Cliente() throws IOException{
		socket = new Socket(HOST,PORT);
	}
	
	/**
	 * Metodo principal con la conexión con el servidor y el flujo de preguntas y respuestas con este.
	 * @throws IOException
	 */
	public void interactua() throws IOException {
		
		// -- CONEXION CON EL SERVIDOR ----------------------------------------------------------------------------------------
		
		// Declarar clases para leer y escribir, la Scanner para los mensajes del usuario y las de input/outputStream
		// del flujo del socket, para enviar y recibir del/al servidor.
		Scanner sc = new Scanner(System.in);
	    DataInputStream entradaDelServidor = new DataInputStream(socket.getInputStream());
	    DataOutputStream salidaAlServidor = new DataOutputStream(socket.getOutputStream());
	    
	    // Leemos el mensaje de bienvenida del servidor
	    System.out.println(entradaDelServidor.readUTF()); 
	    
	    // -- FLUJO DE MENSAJES CON EL SERVIDOR -------------------------------------------------------------------------------

	    // Solicitamos al usuario que ingrese su nombre
	    String nombreCliente = sc.nextLine();

	    // Enviamos el nombre del usuario al servidor
	    salidaAlServidor.writeUTF(nombreCliente);
	    salidaAlServidor.flush();

	    // Leemos el mensaje de bienvenida personalizado del servidor
	    System.out.println(entradaDelServidor.readUTF());
	    
	    // Recibimos la pregunta de las minusculas del servidor. La respondemos y enviamos la respuesta
	    System.out.println(entradaDelServidor.readUTF());
	    int numMinusculas = RequisitosPass.pideNum(sc);
	    salidaAlServidor.write(numMinusculas);
	    salidaAlServidor.flush();
	    
	    // Recibimos la pregunta de las mayusculas del servidor. La respondemos y enviamos la respuesta
	    System.out.println(entradaDelServidor.readUTF());
	    int numMayusculas = RequisitosPass.pideNum(sc);
	    salidaAlServidor.write(numMayusculas);
	    salidaAlServidor.flush();
	    
	    // Recibimos la pregunta de los dígitos del servidor. La respondemos y enviamos la respuesta
	    System.out.println(entradaDelServidor.readUTF());
	    int numDigitos = RequisitosPass.pideNum(sc);
	    salidaAlServidor.write(numDigitos);
	    salidaAlServidor.flush();
	    
	    // Recibimos la pregunta de las carácteres especiales del servidor. La respondemos y enviamos la respuesta
	    System.out.println(entradaDelServidor.readUTF());
	    int numEspeciales = RequisitosPass.pideNum(sc);
	    salidaAlServidor.write(numEspeciales);
	    salidaAlServidor.flush();
	    
	    // Limpiamos el scanner para que luego  al leer un nextLine no tenga saltos de linea donde no debe.
	    // Esta parte me dio problemas hasta que leí en internet el porque de lo que sucedia. 
	    // Lo leí de aqui --> https://es.stackoverflow.com/questions/189703/problema-con-el-salto-de-una-línea-de-código-al-usar-scanner
	    sc.nextLine();
	    
	    // Recibimos la especificación del tamaño de la contraseña y la pregunta sobre si queremos generar la contraseña, esperando una respuesta de si o no
	    System.out.println(entradaDelServidor.readUTF());
	    String respuesta = RequisitosPass.siOno(sc);
	    salidaAlServidor.writeUTF(respuesta);
	    salidaAlServidor.flush();
	    
	    // Obtenemos el mensaje de despedida o con la contraseña, según se haya contestado previamente.
	    System.out.println(entradaDelServidor.readUTF());

	    // -- CERRAR CONEXION DEL CLIENTE ------------------------------------------------------------------------------------
	    
	    // Cerrar recursos abiertos
	    sc.close();
	    entradaDelServidor.close();
	    salidaAlServidor.close();
	    socket.close();
	}

	public String getHOST() {
		return HOST;
	}

	public int getPORT() {
		return PORT;
	}
	
	public Socket getSocket() {
		return socket;
	}
		
}
