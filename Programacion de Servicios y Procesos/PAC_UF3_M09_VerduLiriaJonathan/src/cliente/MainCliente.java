package cliente;

import java.io.IOException;

public class MainCliente {

	public static void main(String[] args) throws IOException{
		
		// Creamos el objeto Cliente
		Cliente c = new Cliente();
		
		// Iniciamos la conexion
		c.interactua();

	}

}
