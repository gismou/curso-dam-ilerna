/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package priorities;

/**
 *
 * @author gismo
 */
public class VT03_DAM_M9_EJ1 extends Thread{
	
    public static void main(String[] args) {
    	Thread.currentThread().setName("Principal");
    	System.out.println(Thread.currentThread().toString());
    	int numHilosCreados = 3;
    	
    	Hilo h = null;
        for(int i=0; i<numHilosCreados; i++){          
            h = new Hilo();
            h.setName("Hilo "+i);
            h.setPriority(i+1);
        	h.start();       
        }
        
        System.out.println(numHilosCreados+" hilos creados...");
        System.out.println("Hilos activos: "+Thread.activeCount());
    }
}
