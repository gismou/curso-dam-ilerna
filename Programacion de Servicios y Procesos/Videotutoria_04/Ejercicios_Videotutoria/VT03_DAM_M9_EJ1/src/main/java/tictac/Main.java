package tictac;

public class Main {

	public static void main(String[] args) {

		boolean salir = false;
		while(salir != true) {
			try {
				Tic tic = new Tic();
				tic.start();
				Thread.sleep(1000);
				Tac tac = new Tac();
				tac.start();
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}
	}

}
