package atletas;

public class HiloAtleta extends Thread{
	int numAtleta;
	int metrosRecorridos;
	
	public HiloAtleta(int numAtleta) {
		this.numAtleta = numAtleta;
		this.metrosRecorridos = 0;
	}
	
	@Override
	public void run(){
		int metrosAleatorio = (int) (Math.floor(Math.random()*(100-0+1)+0));
		
		while(metrosRecorridos < 100) {
			metrosRecorridos += metrosAleatorio;
			if(metrosRecorridos >= 100) {
				metrosRecorridos = 100; // ya ha llegado o sobrepasado la meta
			}else {
				System.out.println("El atleta "+numAtleta+" recorre "+metrosAleatorio+" metros mas, habiendo recorrido "+metrosRecorridos+" metros!");
			}
		}
		System.out.println("!!El atleta "+numAtleta+" ha llegado a la meta!!");

	}
}
