package atletas;

public class MainAtleta extends Thread{

	public static void main(String[] args) throws InterruptedException {
		System.out.println("La carrera ha comenzado");
		
		HiloAtleta[] atletas = new HiloAtleta[10];
		
		// Se inicializan todos los hilos y los guardamos en un array
		for(int i=0; i<10; i++) {
			atletas[i] = new HiloAtleta(i+1);
			atletas[i].start(); // IMPORTANTE, no es lo mismo .start que punto .run
								// CREO que .start inicializa el hilo, pero no espera a que acabe, sigue el 
								// funcionamiento del hilo principal, en este caso iniciando otros hilos
								// y por cada hilo iniciado empieza a su vez el codigo dentro de run.
								// Pero si lo inicializamos con start los hilos, se produce el  multithread.
								// Si lo hago con .run, se espera a que se lance todo el codigo del run antes
								// de seguir en el hilo principal (el main), no provocando el multithread.
		}
		
		// Recorremos el array de hilos esperando que todos acaben (con join).
		for(Thread atleta : atletas) {
			atleta.join();
		}
		
		// Al haber esperado que acaben el resto de hilos, este mensaje aparece al final.
		System.out.println("La carrera ha finalizado");
	}

}
