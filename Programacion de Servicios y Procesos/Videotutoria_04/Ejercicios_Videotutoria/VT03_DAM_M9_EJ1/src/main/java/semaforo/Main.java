package semaforo;

public class Main {

	public static void main(String[] args) {
		Semaforo semaforo = new Semaforo();
		Carretera carreteraNS = new Carretera("NS", semaforo);
		Carretera carreteraEO = new Carretera("EO", semaforo);
		
		carreteraNS.start();
		carreteraEO.start();
	}

}
