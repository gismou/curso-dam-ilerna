package semaforo;

public class Semaforo {
	
	private String viaAbierta = "NS";
	
	public synchronized void waitFor(String via) throws InterruptedException {
		// Si nuestra via no es la que está abierta, entonces esperamos
		while(!via.equals(viaAbierta)) {
			wait();
		}
	}
	
	public synchronized void toggleTurn() {
		// Operador ternario. Si la via abierta es la NS, ponemos la variable como EO
		// Si no es NS, entonces la ponemos como NS.
		viaAbierta = "NS".equals(viaAbierta) ? "EO" : "NS";
		notifyAll();
	}
	
}
