package semaforo;

import java.util.concurrent.ThreadLocalRandom;

public class Carretera extends Thread {
	private String via;
	private Semaforo semaforo;
	
	public Carretera(String via, Semaforo semaforo) {
		this.via = via;
		this.semaforo = semaforo;
	}
	
	public void run() {
		while(true) {
			try {
				// Esperamos a que nuestra via se abra
				semaforo.waitFor(via);
				// Se pone el semáforo en verde para la via. El sleep sirve para poder ver algo mas claro qué está pasando
				System.out.println("Via "+via+" está en verde.");
				sleep(1000);
				// Generamos un numero aleatorio para mostrar que pasan esos coches
				int numCoches = ThreadLocalRandom.current().nextInt(0,6);
				for(int i=0; i<numCoches; i++) {
					System.out.println("Coche "+(i+1)+" pasa por "+via);
				}
				// Se pone el semáforo en rojo para la vía
				sleep(1000);
				System.out.println("Via "+via+" está en rojo.");
				// Notificamos el cambio de vía
				semaforo.toggleTurn();
			}catch(InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
