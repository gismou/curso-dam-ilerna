package saludos;

import java.util.ArrayList;
import java.util.List;

public class MainThread {
	public static void main(String[] args) throws InterruptedException {
		List<SaludoThread> listaSaludos = new ArrayList<>();
		for(int i=0; i<5; i++) {
			// Iniciamos y creamos todos los hilos, guardandolos
			// en una lista de hilos
			SaludoThread saludoThread = new SaludoThread(i+1);
			saludoThread.start();
			listaSaludos.add(saludoThread);
			// Si ponemos el join aquí, esperaria a que acabe el unico hilo
			// que hay creado (por estar en el for).
			//saludoThread.join(); // join() => Espera a que el hilo acabe
		}
		
		// recorremos la lista de hilos (que ya se han lanzado y creado)
		// pero esta vez solo para que espere a que cada uno de ellos acabe 
		// (que recordemos que previamente ya habrian empezado)
		for(Thread saludo : listaSaludos) {
			saludo.join();
		}
		
		// Como hemos dicho que espere a que cada hilo de los creados acabe,
		// este mensaje del hilo principal será el ultimo en salir.
		System.out.println("Fin de la ejecucion del programa");
	}
}
