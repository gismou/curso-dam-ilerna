package com.example.recetaapp20

data class Favorita(
    val id: String = "",  // Este campo es el ID del documento en Firestore
    val id_usuario: String = "",
    val id_receta: String = ""
)