package com.example.recetaapp20

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.FirebaseFirestore

class RegisterActivity : AppCompatActivity() {

    private lateinit var firestore: FirebaseFirestore

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        firestore = FirebaseFirestore.getInstance()

        val userField = findViewById<EditText>(R.id.editTextName)
        val emailField = findViewById<EditText>(R.id.editTextEmailRegister)
        val passwordField = findViewById<EditText>(R.id.editTextPasswordRegister)
        val registerButton = findViewById<Button>(R.id.btnRegister)
        val backToLoginButton = findViewById<Button>(R.id.btnBackToLogin)

        backToLoginButton.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }

        registerButton.setOnClickListener {
            val username = userField.text.toString()
            val email = emailField.text.toString()
            val password = passwordField.text.toString()
            Log.d("RegisterActivity", "User: $username, Email: $email")

            // Verificar que los campos no estén vacíos
            if (username.isNotEmpty() && email.isNotEmpty() && password.isNotEmpty()) {

                // Verificar si el nombre de usuario ya existe
                firestore.collection("Usuarios").document(username)
                    .get()
                    .addOnSuccessListener { document ->
                        if (document.exists()) {
                            // Si el nombre de usuario ya existe, mostrar un mensaje
                            Toast.makeText(this, "El usuario ya existe, elige otro nombre", Toast.LENGTH_SHORT).show()
                        } else {
                            // Verificar si el correo electrónico ya está registrado
                            firestore.collection("Usuarios")
                                .whereEqualTo("email", email)
                                .get()
                                .addOnSuccessListener { emailSnapshot ->
                                    if (!emailSnapshot.isEmpty) {
                                        // Si el correo electrónico ya existe, mostrar un mensaje
                                        Toast.makeText(this, "El correo electrónico ya está registrado", Toast.LENGTH_SHORT).show()
                                    } else {
                                        // Si el usuario y correo no existen, registrar al nuevo usuario
                                        val user = hashMapOf(
                                            "usuario" to username,
                                            "email" to email,
                                            "password" to password
                                        )

                                        // Guardar en la colección "Usuarios"
                                        firestore.collection("Usuarios").document(username)
                                            .set(user)
                                            .addOnCompleteListener {
                                                if (it.isSuccessful) {
                                                    Toast.makeText(this, "Registro exitoso", Toast.LENGTH_SHORT).show()
                                                    // Ir a la pantalla de login
                                                    val intent = Intent(this, LoginActivity::class.java)
                                                    startActivity(intent)
                                                    finish()  // Cerrar la actividad actual
                                                } else {
                                                    Toast.makeText(this, "Error al guardar en la base de datos", Toast.LENGTH_SHORT).show()
                                                }
                                            }
                                    }
                                }
                        }
                    }
            } else {
                // Si algún campo está vacío, mostrar un mensaje de error
                Toast.makeText(this, "Por favor completa todos los campos", Toast.LENGTH_SHORT).show()
            }
        }
    }
}
