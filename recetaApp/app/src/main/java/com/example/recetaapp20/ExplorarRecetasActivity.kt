package com.example.recetaapp20;

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.SearchView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.FirebaseFirestore

class ExplorarRecetasActivity : AppCompatActivity() {

    private lateinit var db: FirebaseFirestore
    private lateinit var recetasRecyclerView: RecyclerView
    private lateinit var recetasAdapter: RecetasAdapter
    private val recetasList = mutableListOf<Receta>()
    private lateinit var searchView: SearchView
    private lateinit var btnCancelar: Button
    private lateinit var btnMostrarFavoritos: Button

    private var showFavoritos = false
    private var favoritosSet = mutableSetOf<String>() // Set que almacenará las recetas favoritas del usuario

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_explorar_recetas)

        db = FirebaseFirestore.getInstance()
        recetasRecyclerView = findViewById(R.id.recetasRecyclerView)
        btnCancelar = findViewById(R.id.btnBackToLogin)
        btnMostrarFavoritos = findViewById(R.id.btnMostrarFavoritos)
        searchView = findViewById(R.id.searchView)

        // Extras recibidos
        val username = intent.getStringExtra("USERNAME")
        val user_id = intent.getStringExtra("USER_ID")

        btnCancelar.setOnClickListener {
            intent.putExtra("USERNAME", username)
            intent.putExtra("USER_ID", user_id)
            finish() // Volver a la actividad anterior
        }

        recetasAdapter = RecetasAdapter(
            recetasList,
            userId = user_id ?: "",
            esExplorar = true,
            onItemClick = { receta ->
                // Ir al DetalleRecetaActivity
                val intent = Intent(this, DetalleRecetaActivity::class.java)
                intent.putExtra("RECETA_ID", receta.id)
                startActivity(intent)
            }
        )

        recetasRecyclerView.layoutManager = LinearLayoutManager(this)
        recetasRecyclerView.adapter = recetasAdapter

        cargarRecetas() // Cargar recetas de Firebase
        cargarFavoritos() // Cargar las recetas favoritas del Usuario

        // Configurar el listener de búsqueda
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                recetasAdapter.filter.filter(query)
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                recetasAdapter.filter.filter(newText)
                return true
            }
        })

        // Configurar el botón para alternar entre todas las recetas y solo las favoritas
        btnMostrarFavoritos.setOnClickListener {
            cargarFavoritos()
            showFavoritos = !showFavoritos
            btnMostrarFavoritos.text = if (showFavoritos) "Ver Todas" else "Ver Favoritos"
            recetasAdapter = RecetasAdapter(
                recetasList,
                userId = user_id ?: "",
                esExplorar = true,
                showFavoritos = showFavoritos, // Pasar el filtro de favoritos
                onItemClick = { receta ->
                    val intent = Intent(this, DetalleRecetaActivity::class.java)
                    intent.putExtra("RECETA_ID", receta.id)
                    startActivity(intent)
                }
            )
            recetasRecyclerView.adapter = recetasAdapter
            recetasAdapter.updateFavoritosFilter(favoritosSet)
        }
    }

    private fun cargarRecetas() {
        db.collection("recetas")
            .get()
            .addOnSuccessListener { documents ->
                recetasList.clear()
                for (document in documents) {
                    val receta = document.toObject(Receta::class.java).copy(id = document.id)
                    recetasList.add(receta)
                }
                recetasAdapter.notifyDataSetChanged() // Actualizar la lista inicial
            }
            .addOnFailureListener { e ->
                Toast.makeText(this, "Error al cargar las recetas: ${e.message}", Toast.LENGTH_SHORT).show()
            }
    }

    // Obtener favoritos del usuario desde Firestore
    private fun cargarFavoritos() {
        val user_id = intent.getStringExtra("USER_ID") ?: return
        db.collection("favoritas")
            .whereEqualTo("id_usuario", user_id)
            .get()
            .addOnSuccessListener { documents ->
                favoritosSet.clear() // Limpiamos la lista de favoritos
                for (document in documents) {
                    val recetaId = document.getString("id_receta")
                    if (recetaId != null) {
                        favoritosSet.add(recetaId) // Agregar receta a favoritos
                    }
                }
                recetasAdapter.updateFavoritosFilter(favoritosSet) // Actualizar el filtro de favoritos
            }
            .addOnFailureListener { e ->
                Toast.makeText(this, "Error al cargar los favoritos: ${e.message}", Toast.LENGTH_SHORT).show()
            }
    }
}
