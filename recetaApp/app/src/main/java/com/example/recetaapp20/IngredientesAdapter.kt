package com.example.recetaapp20

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class IngredientesAdapter(
    private val ingredientes: List<Ingrediente>,
    private val onItemClick: (Ingrediente) -> Unit,
    private val onDeleteClick: (Ingrediente) -> Unit
) : RecyclerView.Adapter<IngredientesAdapter.IngredienteViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IngredienteViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_ingrediente, parent, false)
        return IngredienteViewHolder(view)
    }

    override fun onBindViewHolder(holder: IngredienteViewHolder, position: Int) {
        val ingrediente = ingredientes[position]
        holder.bind(ingrediente, onItemClick, onDeleteClick)
    }

    override fun getItemCount() = ingredientes.size

    class IngredienteViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val ingredienteNombre: TextView = itemView.findViewById(R.id.ingredienteNombre)
        private val ingredienteTipoUnidad: TextView = itemView.findViewById(R.id.ingredienteTipoUnidad)
        private val btnEliminar: Button = itemView.findViewById(R.id.btnEliminarIngrediente)

        fun bind(
            ingrediente: Ingrediente,
            onItemClick: (Ingrediente) -> Unit,
            onDeleteClick: (Ingrediente) -> Unit
        ) {
            ingredienteNombre.text = ingrediente.nombre
            ingredienteTipoUnidad.text = ingrediente.tipoUnidad
            itemView.setOnClickListener { onItemClick(ingrediente) }
            btnEliminar.setOnClickListener { onDeleteClick(ingrediente) }
        }
    }
}
