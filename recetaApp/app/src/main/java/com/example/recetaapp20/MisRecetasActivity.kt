package com.example.recetaapp20

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.FirebaseFirestore

class MisRecetasActivity : AppCompatActivity() {

    private lateinit var db: FirebaseFirestore
    private lateinit var recetasRecyclerView: RecyclerView
    private lateinit var recetasAdapter: RecetasAdapter
    private val recetasList = mutableListOf<Receta>()

    // Definir un código de solicitud
    private val REQUEST_CODE_EDITAR_RECETA = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mis_recetas)

        db = FirebaseFirestore.getInstance()
        recetasRecyclerView = findViewById(R.id.recetasRecyclerView)

        // Extras
        val username = intent.getStringExtra("USERNAME")
        val user_id = intent.getStringExtra("USER_ID")

        // Configurar el adaptador con clic para editar y eliminar
        recetasAdapter = RecetasAdapter(
            recetasList,
            userId = user_id ?: "",
            esExplorar = false,
            onItemClick = { receta ->
                val intent = Intent(this, EditarRecetaActivity::class.java)
                intent.putExtra("RECETA_ID", receta.id)
                intent.putExtra("USERNAME", username)
                intent.putExtra("USER_ID", user_id)
                startActivityForResult(intent, REQUEST_CODE_EDITAR_RECETA)  // Usar startActivityForResult
            },
            onDeleteClick = { receta ->
                eliminarReceta(receta.id)  // Permitir eliminar la receta
            }
        )

        recetasRecyclerView.layoutManager = LinearLayoutManager(this)
        recetasRecyclerView.adapter = recetasAdapter

        val btnAtras = findViewById<Button>(R.id.btnBackToLogin)
        btnAtras.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("USERNAME", username)
            intent.putExtra("USER_ID", user_id)
            startActivity(intent)
        }

        val btnAgregarReceta = findViewById<Button>(R.id.btnAgregarReceta)
        btnAgregarReceta.setOnClickListener {
            val intent = Intent(this, EditarRecetaActivity::class.java)
            intent.putExtra("USERNAME", username)
            intent.putExtra("USER_ID", user_id)
            startActivityForResult(intent, REQUEST_CODE_EDITAR_RECETA)  // Usar startActivityForResult
        }

        cargarRecetasUsuario(user_id)
    }

    private fun cargarRecetasUsuario(user_id: String?) {
        db.collection("recetas")
            .whereEqualTo("usuario", user_id)
            .get()
            .addOnSuccessListener { documents ->
                recetasList.clear()
                for (document in documents) {
                    val receta = document.toObject(Receta::class.java).copy(id = document.id)
                    recetasList.add(receta)
                }
                recetasAdapter.notifyDataSetChanged()
            }
            .addOnFailureListener { e ->
                Toast.makeText(this, "Error al cargar las recetas: ${e.message}", Toast.LENGTH_SHORT).show()
            }
    }

    private fun eliminarReceta(recetaId: String) {
        db.collection("recetas").document(recetaId)
            .delete()
            .addOnSuccessListener {
                Toast.makeText(this, "Receta eliminada", Toast.LENGTH_SHORT).show()
                val user_id = intent.getStringExtra("USER_ID")
                cargarRecetasUsuario(user_id)  // Recargar recetas después de eliminar
            }
            .addOnFailureListener { e ->
                Toast.makeText(this, "Error al eliminar la receta: ${e.message}", Toast.LENGTH_SHORT).show()
            }
    }

    // Sobrescribir onActivityResult() para manejar el resultado al volver de EditarRecetaActivity
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_CODE_EDITAR_RECETA && resultCode == RESULT_OK) {
            val recetaGuardada = data?.getBooleanExtra("RECETA_GUARDADA", false) ?: false
            if (recetaGuardada) {
                val user_id = intent.getStringExtra("USER_ID")
                if (user_id != null) {
                    cargarRecetasUsuario(user_id)  // Recargar recetas desde la base de datos
                }
            }
        }
    }
}
