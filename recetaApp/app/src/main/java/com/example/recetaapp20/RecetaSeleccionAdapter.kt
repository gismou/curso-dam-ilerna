package com.example.recetaapp20

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class RecetaSeleccionAdapter(
    private val recetas: List<Receta>,
    private val onRecetaChecked: (Receta, Boolean) -> Unit
) : RecyclerView.Adapter<RecetaSeleccionAdapter.RecetaViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecetaViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_receta_seleccion, parent, false)
        return RecetaViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecetaViewHolder, position: Int) {
        holder.bind(recetas[position], onRecetaChecked)
    }

    override fun getItemCount() = recetas.size

    class RecetaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val tvNombre: TextView = itemView.findViewById(R.id.tvRecetaNombre)
        private val checkBox: CheckBox = itemView.findViewById(R.id.cbSeleccionReceta)

        fun bind(receta: Receta, onRecetaChecked: (Receta, Boolean) -> Unit) {
            tvNombre.text = receta.nombre
            checkBox.setOnCheckedChangeListener(null)
            checkBox.isChecked = false // Reset checkbox state
            checkBox.setOnCheckedChangeListener { _, isChecked ->
                onRecetaChecked(receta, isChecked)
            }
        }
    }
}
