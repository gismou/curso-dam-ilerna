package com.example.recetaapp20

data class Ingrediente(
    val id: String = "",  // Este campo es el ID del documento en Firestore
    val nombre: String = "",
    val tipoUnidad: String = "",
    val usuario: String = ""
)
{
    override fun toString(): String {
        return nombre
    }
}