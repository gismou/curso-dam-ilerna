package com.example.recetaapp20

data class Receta(
    val id: String = "", // ID de la receta (esto se asigna después al obtener el documento)
    val nombre: String = "",
    val tiempoMins: Int = 0,
    val usuario: String = "",
    val dificultad: String = "",
    val instrucciones: List<String> = listOf(),
    val ingredientes: List<String> = listOf() // Lista de IDs de ingredientes
) {
    // Sobrescribimos el método toString para que solo muestre el nombre
    override fun toString(): String {
        return nombre
    }
}