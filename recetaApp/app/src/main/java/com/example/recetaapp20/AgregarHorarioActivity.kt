package com.example.recetaapp20

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.FirebaseFirestore

class AgregarHorarioActivity : AppCompatActivity() {

    private lateinit var db: FirebaseFirestore
    private var user_id: String? = null
    private var username: String? = null

    private lateinit var btnCancelar: Button

    // Spinners para cada comida en cada día de la semana
    private lateinit var spinnerDesayunoLunes: Spinner
    private lateinit var spinnerAlmuerzoLunes: Spinner
    private lateinit var spinnerComidaLunes: Spinner
    private lateinit var spinnerMeriendaLunes: Spinner
    private lateinit var spinnerCenaLunes: Spinner

    private lateinit var spinnerDesayunoMartes: Spinner
    private lateinit var spinnerAlmuerzoMartes: Spinner
    private lateinit var spinnerComidaMartes: Spinner
    private lateinit var spinnerMeriendaMartes: Spinner
    private lateinit var spinnerCenaMartes: Spinner

    private lateinit var spinnerDesayunoMiercoles: Spinner
    private lateinit var spinnerAlmuerzoMiercoles: Spinner
    private lateinit var spinnerComidaMiercoles: Spinner
    private lateinit var spinnerMeriendaMiercoles: Spinner
    private lateinit var spinnerCenaMiercoles: Spinner

    private lateinit var spinnerDesayunoJueves: Spinner
    private lateinit var spinnerAlmuerzoJueves: Spinner
    private lateinit var spinnerComidaJueves: Spinner
    private lateinit var spinnerMeriendaJueves: Spinner
    private lateinit var spinnerCenaJueves: Spinner

    private lateinit var spinnerDesayunoViernes: Spinner
    private lateinit var spinnerAlmuerzoViernes: Spinner
    private lateinit var spinnerComidaViernes: Spinner
    private lateinit var spinnerMeriendaViernes: Spinner
    private lateinit var spinnerCenaViernes: Spinner

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_agregar_horario)
        btnCancelar = findViewById(R.id.btnBackToLogin)

        // Inicializamos Firestore
        db = FirebaseFirestore.getInstance()

        // Extras
        username = intent.getStringExtra("USERNAME")
        user_id = intent.getStringExtra("USER_ID")

        btnCancelar.setOnClickListener {
            intent.putExtra("USERNAME", username)
            intent.putExtra("USER_ID", user_id)
            finish()  // Termina la actividad y vuelve a la actividad anterior
        }

        // Enlazamos los Spinners para todos los días de la semana
        spinnerDesayunoLunes = findViewById(R.id.spinnerDesayunoLunes)
        spinnerAlmuerzoLunes = findViewById(R.id.spinnerAlmuerzoLunes)
        spinnerComidaLunes = findViewById(R.id.spinnerComidaLunes)
        spinnerMeriendaLunes = findViewById(R.id.spinnerMeriendaLunes)
        spinnerCenaLunes = findViewById(R.id.spinnerCenaLunes)

        spinnerDesayunoMartes = findViewById(R.id.spinnerDesayunoMartes)
        spinnerAlmuerzoMartes = findViewById(R.id.spinnerAlmuerzoMartes)
        spinnerComidaMartes = findViewById(R.id.spinnerComidaMartes)
        spinnerMeriendaMartes = findViewById(R.id.spinnerMeriendaMartes)
        spinnerCenaMartes = findViewById(R.id.spinnerCenaMartes)

        spinnerDesayunoMiercoles = findViewById(R.id.spinnerDesayunoMiercoles)
        spinnerAlmuerzoMiercoles = findViewById(R.id.spinnerAlmuerzoMiercoles)
        spinnerComidaMiercoles = findViewById(R.id.spinnerComidaMiercoles)
        spinnerMeriendaMiercoles = findViewById(R.id.spinnerMeriendaMiercoles)
        spinnerCenaMiercoles = findViewById(R.id.spinnerCenaMiercoles)

        spinnerDesayunoJueves = findViewById(R.id.spinnerDesayunoJueves)
        spinnerAlmuerzoJueves = findViewById(R.id.spinnerAlmuerzoJueves)
        spinnerComidaJueves = findViewById(R.id.spinnerComidaJueves)
        spinnerMeriendaJueves = findViewById(R.id.spinnerMeriendaJueves)
        spinnerCenaJueves = findViewById(R.id.spinnerCenaJueves)

        spinnerDesayunoViernes = findViewById(R.id.spinnerDesayunoViernes)
        spinnerAlmuerzoViernes = findViewById(R.id.spinnerAlmuerzoViernes)
        spinnerComidaViernes = findViewById(R.id.spinnerComidaViernes)
        spinnerMeriendaViernes = findViewById(R.id.spinnerMeriendaViernes)
        spinnerCenaViernes = findViewById(R.id.spinnerCenaViernes)

        // Cargar recetas en los spinners
        cargarRecetas()

        // Configuración del botón de guardar
        findViewById<Button>(R.id.btnGuardarHorario).setOnClickListener {
            guardarHorario()
        }
    }

    // Función para mostrar/ocultar las opciones del día
    fun toggleLunes(view: View) {
        val llLunes = findViewById<LinearLayout>(R.id.llLunes)
        llLunes.visibility = if (llLunes.visibility == View.VISIBLE) View.GONE else View.VISIBLE
    }

    fun toggleMartes(view: View) {
        val llMartes = findViewById<LinearLayout>(R.id.llMartes)
        llMartes.visibility = if (llMartes.visibility == View.VISIBLE) View.GONE else View.VISIBLE
    }

    fun toggleMiercoles(view: View) {
        val llMiercoles = findViewById<LinearLayout>(R.id.llMiercoles)
        llMiercoles.visibility = if (llMiercoles.visibility == View.VISIBLE) View.GONE else View.VISIBLE
    }

    fun toggleJueves(view: View) {
        val llJueves = findViewById<LinearLayout>(R.id.llJueves)
        llJueves.visibility = if (llJueves.visibility == View.VISIBLE) View.GONE else View.VISIBLE
    }

    fun toggleViernes(view: View) {
        val llViernes = findViewById<LinearLayout>(R.id.llViernes)
        llViernes.visibility = if (llViernes.visibility == View.VISIBLE) View.GONE else View.VISIBLE
    }

    // Función para cargar las recetas en los spinners
    private fun cargarRecetas() {
        // Verificar que el user_id no es nulo
        if (user_id.isNullOrEmpty()) {
            Toast.makeText(this, "ID de usuario no disponible", Toast.LENGTH_SHORT).show()
            return
        }

        // Lista para almacenar las recetas combinadas (del usuario y las públicas)
        val recetas = mutableListOf("Sin definir") // Añadimos "Sin definir" por defecto

        // Primero, cargamos las recetas del usuario actual (favoritas o creadas)
        db.collection("recetas")
            .whereEqualTo("usuarioId", user_id) // Filtramos por el usuarioId
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    val receta = document.getString("nombre") ?: ""
                    if (receta.isNotEmpty()) {
                        recetas.add(receta)
                    }
                }

                // Luego, cargamos las recetas públicas o de otros usuarios en los favoritos
                db.collection("recetas") // Suponiendo que la colección "recetas" tiene todas las recetas
                    .get()
                    .addOnSuccessListener { allRecipes ->
                        for (document in allRecipes) {
                            val receta = document.getString("nombre") ?: ""
                            if (receta.isNotEmpty() && !recetas.contains(receta)) {
                                recetas.add(receta) // Agregamos solo recetas que no estén ya en la lista
                            }
                        }

                        // Crear el adapter con todas las recetas obtenidas
                        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, recetas)
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

                        // Asignar el mismo adapter a todos los spinners
                        spinnerDesayunoLunes.adapter = adapter
                        spinnerAlmuerzoLunes.adapter = adapter
                        spinnerComidaLunes.adapter = adapter
                        spinnerMeriendaLunes.adapter = adapter
                        spinnerCenaLunes.adapter = adapter

                        spinnerDesayunoMartes.adapter = adapter
                        spinnerAlmuerzoMartes.adapter = adapter
                        spinnerComidaMartes.adapter = adapter
                        spinnerMeriendaMartes.adapter = adapter
                        spinnerCenaMartes.adapter = adapter

                        spinnerDesayunoMiercoles.adapter = adapter
                        spinnerAlmuerzoMiercoles.adapter = adapter
                        spinnerComidaMiercoles.adapter = adapter
                        spinnerMeriendaMiercoles.adapter = adapter
                        spinnerCenaMiercoles.adapter = adapter

                        spinnerDesayunoJueves.adapter = adapter
                        spinnerAlmuerzoJueves.adapter = adapter
                        spinnerComidaJueves.adapter = adapter
                        spinnerMeriendaJueves.adapter = adapter
                        spinnerCenaJueves.adapter = adapter

                        spinnerDesayunoViernes.adapter = adapter
                        spinnerAlmuerzoViernes.adapter = adapter
                        spinnerComidaViernes.adapter = adapter
                        spinnerMeriendaViernes.adapter = adapter
                        spinnerCenaViernes.adapter = adapter

                        // Ahora buscamos las recetas asociadas al usuario en la colección "horario_semana"
                        cargarHorarioUsuario()
                    }
                    .addOnFailureListener {
                        Toast.makeText(this, "Error al cargar recetas públicas", Toast.LENGTH_SHORT).show()
                    }

            }
            .addOnFailureListener {
                Toast.makeText(this, "Error al cargar las recetas del usuario", Toast.LENGTH_SHORT).show()
            }
    }


    private fun cargarHorarioUsuario() {
        if (user_id.isNullOrEmpty()) {
            Toast.makeText(this, "ID de usuario no disponible", Toast.LENGTH_SHORT).show()
            return
        }

        // Accedemos a la colección "horario_semana" para obtener las recetas asociadas al usuario
        db.collection("horario_semana")
            .whereEqualTo("usuarioId", user_id)
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    val dia = document.getString("dia") ?: ""
                    val horaDia = document.getString("hora_dia") ?: ""
                    val receta = document.getString("receta") ?: ""

                    // Ahora verificamos qué spinner corresponde a este día y hora
                    val spinner = getSpinner(dia, horaDia)
                    if (spinner != null && receta.isNotEmpty()) {
                        val adapter = spinner.adapter as ArrayAdapter<String>
                        val position = adapter.getPosition(receta)
                        if (position != -1) {
                            spinner.setSelection(position) // Establecemos la receta seleccionada en el spinner
                        }
                    }
                }
            }
            .addOnFailureListener {
                Toast.makeText(this, "Error al cargar el horario del usuario", Toast.LENGTH_SHORT).show()
            }
    }



    // Función para guardar los horarios
    private fun guardarHorario() {
        Log.d("GuardarHorario", "Guardando horario...") // Este log ayuda a saber si la función se llama

        // Crear un HashMap para guardar los horarios
        val horarioMap = mutableListOf<HashMap<String, Any>>()

        // Días de la semana
        val dias = listOf("Lunes", "Martes", "Miércoles", "Jueves", "Viernes")
        // Tipos de comida
        val comidas = listOf("Desayunar", "Almorzar", "Comer", "Merendar", "Cenar")

        // Iterar por días y comidas
        dias.forEach { dia ->
            comidas.forEach { comida ->
                // Obtener el spinner correspondiente
                val spinner = getSpinner(dia, comida)
                val recetaSeleccionada = spinner?.selectedItem?.toString() ?: "" // Asegurarse que no sea null

                Log.d("GuardarHorario", "Día: $dia, Comida: $comida, Receta seleccionada: $recetaSeleccionada") // Verifica lo que se está recolectando

                // Si se ha seleccionado una receta o "Sin definir", la agregamos al horarioMap
                if (recetaSeleccionada.isNotEmpty()) {
                    val horario = hashMapOf<String, Any>(
                        "usuarioId" to (user_id ?: ""), // Garantizamos que user_id no sea null
                        "dia" to dia,
                        "hora_dia" to comida,
                        "receta" to recetaSeleccionada
                    )

                    // Añadir el horario al mapa
                    horarioMap.add(horario)
                }
            }
        }

        // Verificamos si hay horarios para guardar
        if (horarioMap.isNotEmpty()) {
            // Si hay horarios, los agregamos uno por uno a Firestore
            for (horario in horarioMap) {
                db.collection("horario_semana")
                    .add(horario) // Aquí estamos agregando cada horario de forma individual
                    .addOnSuccessListener {
                        Log.d("GuardarHorario", "Horario guardado exitosamente.")
                    }
                    .addOnFailureListener { exception ->
                        Toast.makeText(this, "Error al guardar el horario", Toast.LENGTH_SHORT).show()
                        Log.e("GuardarHorario", "Error al guardar el horario", exception)
                    }
            }
        } else {
            Toast.makeText(this, "Por favor, selecciona recetas para todos los horarios.", Toast.LENGTH_SHORT).show()
            Log.d("GuardarHorario", "No se seleccionaron recetas para guardar.")
        }
    }


    // Función para obtener el spinner correspondiente según el día y la comida
    private fun getSpinner(dia: String, comida: String): Spinner? {
        return when (dia) {
            "Lunes" -> when (comida) {
                "Desayunar" -> spinnerDesayunoLunes
                "Almorzar" -> spinnerAlmuerzoLunes
                "Comer" -> spinnerComidaLunes
                "Merendar" -> spinnerMeriendaLunes
                "Cenar" -> spinnerCenaLunes
                else -> null
            }
            "Martes" -> when (comida) {
                "Desayunar" -> spinnerDesayunoMartes
                "Almorzar" -> spinnerAlmuerzoMartes
                "Comer" -> spinnerComidaMartes
                "Merendar" -> spinnerMeriendaMartes
                "Cenar" -> spinnerCenaMartes
                else -> null
            }
            "Miércoles" -> when (comida) {
                "Desayunar" -> spinnerDesayunoMiercoles
                "Almorzar" -> spinnerAlmuerzoMiercoles
                "Comer" -> spinnerComidaMiercoles
                "Merendar" -> spinnerMeriendaMiercoles
                "Cenar" -> spinnerCenaMiercoles
                else -> null
            }
            "Jueves" -> when (comida) {
                "Desayunar" -> spinnerDesayunoJueves
                "Almorzar" -> spinnerAlmuerzoJueves
                "Comer" -> spinnerComidaJueves
                "Merendar" -> spinnerMeriendaJueves
                "Cenar" -> spinnerCenaJueves
                else -> null
            }
            "Viernes" -> when (comida) {
                "Desayunar" -> spinnerDesayunoViernes
                "Almorzar" -> spinnerAlmuerzoViernes
                "Comer" -> spinnerComidaViernes
                "Merendar" -> spinnerMeriendaViernes
                "Cenar" -> spinnerCenaViernes
                else -> null
            }
            else -> null
        }
    }
}
