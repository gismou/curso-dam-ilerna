package com.example.recetaapp20

import android.os.Bundle
import android.text.TextUtils
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.FirebaseFirestore

class EditarIngredienteActivity : AppCompatActivity() {

    private lateinit var db: FirebaseFirestore
    private lateinit var nombreEditText: EditText
    private lateinit var tipoUnidadEditText: EditText
    private lateinit var btnGuardar: Button
    private lateinit var btnCancelar: Button
    private var ingredienteId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_editar_ingrediente)

        // Extras
        val username = intent.getStringExtra("USERNAME")
        val user_id = intent.getStringExtra("USER_ID")

        db = FirebaseFirestore.getInstance()

        // Inicializar los campos de la UI
        nombreEditText = findViewById(R.id.nombreIngredienteEditText)
        tipoUnidadEditText = findViewById(R.id.tipoUnidadEditText)
        btnGuardar = findViewById(R.id.btnGuardarIngrediente)
        btnCancelar = findViewById(R.id.btnBackToLogin)

        // Obtener el ID del ingrediente (si estamos editando uno)
        ingredienteId = intent.getStringExtra("INGREDIENTE_ID")

        // Si hay un ID de ingrediente, cargar los datos
        if (ingredienteId != null) {
            cargarIngredienteParaEditar(ingredienteId!!)
        }

        // Configurar el botón de guardar para agregar o actualizar el ingrediente
        btnGuardar.setOnClickListener {
            guardarIngrediente()
        }

        // Configurar el botón de cancelar para volver sin guardar
        btnCancelar.setOnClickListener {
            intent.putExtra("USERNAME", username)
            intent.putExtra("USER_ID",user_id)
            finish()  // Termina la actividad y vuelve a la actividad anterior
        }
    }

    // Cargar un ingrediente desde Firestore para editarlo
    private fun cargarIngredienteParaEditar(ingredienteId: String) {
        db.collection("ingredientes").document(ingredienteId).get()
            .addOnSuccessListener { document ->
                if (document.exists()) {
                    val ingrediente = document.toObject(Ingrediente::class.java)
                    // Rellenar los campos de la UI con los datos del ingrediente
                    ingrediente?.let {
                        nombreEditText.setText(it.nombre)
                        tipoUnidadEditText.setText(it.tipoUnidad)
                    }
                } else {
                    Toast.makeText(this, "Ingrediente no encontrado", Toast.LENGTH_SHORT).show()
                }
            }
            .addOnFailureListener { e ->
                Toast.makeText(this, "Error al cargar el ingrediente: ${e.message}", Toast.LENGTH_SHORT).show()
            }
    }

    // Guardar o actualizar el ingrediente en Firestore
    private fun guardarIngrediente() {
        val nombre = nombreEditText.text.toString().trim()
        val tipoUnidad = tipoUnidadEditText.text.toString().trim()

        // Validar que ambos campos no estén vacíos
        if (TextUtils.isEmpty(nombre) || TextUtils.isEmpty(tipoUnidad)) {
            Toast.makeText(this, "Todos los campos son obligatorios", Toast.LENGTH_SHORT).show()
            return
        }

        // Obtener el id_usuario desde los extras
        val user_id = intent.getStringExtra("USER_ID") ?: return

        // Verificar si ya existe un ingrediente con el mismo nombre para este usuario
        db.collection("ingredientes")
            .whereEqualTo("usuario", user_id)
            .whereEqualTo("nombre", nombre)
            .get()
            .addOnSuccessListener { documents ->
                val ingredienteExistente = documents.documents.firstOrNull()

                // Si estamos editando y el ingrediente encontrado tiene el mismo ID, permitimos la edición
                if (ingredienteId != null && ingredienteExistente?.id == ingredienteId) {
                    guardarOActualizarIngrediente(nombre, tipoUnidad, user_id)
                    return@addOnSuccessListener
                }

                // Si ya existe un ingrediente con el mismo nombre para este usuario
                if (ingredienteExistente != null) {
                    Toast.makeText(this, "El ingrediente ya existe para este usuario", Toast.LENGTH_SHORT).show()
                    return@addOnSuccessListener
                }

                // Si no existe, proceder con la creación o actualización
                guardarOActualizarIngrediente(nombre, tipoUnidad, user_id)
            }
            .addOnFailureListener { e ->
                Toast.makeText(this, "Error al verificar el ingrediente: ${e.message}", Toast.LENGTH_SHORT).show()
            }
    }

    private fun guardarOActualizarIngrediente(nombre: String, tipoUnidad: String, userId: String) {
        val ingrediente = Ingrediente(
            nombre = nombre,
            tipoUnidad = tipoUnidad,
            usuario = userId
        )

        if (ingredienteId != null) {
            // Actualizar el ingrediente existente
            db.collection("ingredientes").document(ingredienteId!!)
                .set(ingrediente)
                .addOnSuccessListener {
                    Toast.makeText(this, "Ingrediente actualizado con éxito", Toast.LENGTH_SHORT).show()
                    finish()
                }
                .addOnFailureListener { e ->
                    Toast.makeText(this, "Error al actualizar el ingrediente: ${e.message}", Toast.LENGTH_SHORT).show()
                }
        } else {
            // Crear un nuevo ingrediente
            db.collection("ingredientes").add(ingrediente)
                .addOnSuccessListener {
                    Toast.makeText(this, "Ingrediente guardado con éxito", Toast.LENGTH_SHORT).show()
                    finish()
                }
                .addOnFailureListener { e ->
                    Toast.makeText(this, "Error al guardar el ingrediente: ${e.message}", Toast.LENGTH_SHORT).show()
                }
        }
    }


}
