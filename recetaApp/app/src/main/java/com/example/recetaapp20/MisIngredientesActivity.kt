package com.example.recetaapp20

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.FirebaseFirestore

class MisIngredientesActivity : AppCompatActivity() {

    private lateinit var db: FirebaseFirestore
    private lateinit var ingredientesRecyclerView: RecyclerView
    private lateinit var ingredientesAdapter: IngredientesAdapter
    private val ingredientesList = mutableListOf<Ingrediente>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mis_ingredientes)

        db = FirebaseFirestore.getInstance()
        ingredientesRecyclerView = findViewById(R.id.ingredientesRecyclerView)

        // Extras
        val username = intent.getStringExtra("USERNAME")
        val user_id = intent.getStringExtra("USER_ID")

        // Configurar adaptador con onDeleteClick para eliminar
        ingredientesAdapter = IngredientesAdapter(
            ingredientesList,
            onItemClick = { ingrediente ->
                val intent = Intent(this, EditarIngredienteActivity::class.java)
                intent.putExtra("INGREDIENTE_ID", ingrediente.id)
                intent.putExtra("USERNAME", username)
                intent.putExtra("USER_ID", user_id)
                startActivity(intent)
            },
            onDeleteClick = { ingrediente ->
                eliminarIngrediente(ingrediente.id)
            }
        )

        ingredientesRecyclerView.layoutManager = LinearLayoutManager(this)
        ingredientesRecyclerView.adapter = ingredientesAdapter

        val btnAtras = findViewById<Button>(R.id.btnBackToLogin)
        btnAtras.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("USERNAME", username)
            intent.putExtra("USER_ID", user_id)
            startActivity(intent)
        }

        val btnAgregarIngrediente = findViewById<Button>(R.id.btnAgregarIngrediente)
        btnAgregarIngrediente.setOnClickListener {
            val intent = Intent(this, EditarIngredienteActivity::class.java)
            intent.putExtra("USERNAME", username)
            intent.putExtra("USER_ID", user_id)
            startActivity(intent)
        }

        cargarIngredientesUsuario(user_id)
    }

    override fun onResume() {
        super.onResume()
        val user_id = intent.getStringExtra("USER_ID")
        cargarIngredientesUsuario(user_id)
    }

    private fun cargarIngredientesUsuario(user_id: String?) {
        db.collection("ingredientes")
            .whereEqualTo("usuario", user_id)
            .get()
            .addOnSuccessListener { documents ->
                ingredientesList.clear()
                for (document in documents) {
                    val ingrediente = document.toObject(Ingrediente::class.java).copy(id = document.id)
                    ingredientesList.add(ingrediente)
                }
                ingredientesAdapter.notifyDataSetChanged()
            }
            .addOnFailureListener { e ->
                Toast.makeText(this, "Error al cargar los ingredientes: ${e.message}", Toast.LENGTH_SHORT).show()
            }
    }

    private fun eliminarIngrediente(ingredienteId: String) {
        db.collection("ingredientes").document(ingredienteId)
            .delete()
            .addOnSuccessListener {
                Toast.makeText(this, "Ingrediente eliminado", Toast.LENGTH_SHORT).show()
                val user_id = intent.getStringExtra("USER_ID")
                cargarIngredientesUsuario(user_id) // Recargar ingredientes después de eliminar
            }
            .addOnFailureListener { e ->
                Toast.makeText(this, "Error al eliminar el ingrediente: ${e.message}", Toast.LENGTH_SHORT).show()
            }
    }
}
