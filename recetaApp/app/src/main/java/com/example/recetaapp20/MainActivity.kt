package com.example.recetaapp20

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Extras
        val username = intent.getStringExtra("USERNAME")
        val user_id = intent.getStringExtra("USER_ID")

        val welcomeText = findViewById<TextView>(R.id.welcomeText)
        welcomeText.text = "Bienvenido, $username" // Mostrar el nombre de usuario

        val btnMisRecetas = findViewById<Button>(R.id.btnMisRecetas)
        val btnMisIngredientes = findViewById<Button>(R.id.btnMisIngredientes)
        val btnExplorarRecetas = findViewById<Button>(R.id.btnExplorarRecetas)
        val btnHorarioSemanal = findViewById<Button>(R.id.btnHorarioSemanal)
        val backToLoginButton = findViewById<Button>(R.id.btnBackToLogin)

        backToLoginButton.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            intent.putExtra("USER_ID", user_id)
            intent.putExtra("USERNAME", username)
            startActivity(intent)
            finish()
        }

        // Redirigir a Mis Recetas
        btnMisRecetas.setOnClickListener {
            val intent = Intent(this, MisRecetasActivity::class.java)
            intent.putExtra("USERNAME", username)
            intent.putExtra("USER_ID",user_id)
            startActivity(intent)
        }

        // Redirigir a Mis Ingredientes
        btnMisIngredientes.setOnClickListener {
            val intent = Intent(this, MisIngredientesActivity::class.java)
            intent.putExtra("USERNAME", username)
            intent.putExtra("USER_ID",user_id)
            startActivity(intent)
        }

        // Redirigir a Explorar Recetas
        btnExplorarRecetas.setOnClickListener {
            val intent = Intent(this, ExplorarRecetasActivity::class.java)
            intent.putExtra("USERNAME", username)
            intent.putExtra("USER_ID",user_id)
            startActivity(intent)
        }

        // Redirigir a Horario Semanal
        btnHorarioSemanal.setOnClickListener {
            val intent = Intent(this, AgregarHorarioActivity::class.java)
            intent.putExtra("USERNAME", username)
            intent.putExtra("USER_ID",user_id)
            startActivity(intent)
        }
    }
}
