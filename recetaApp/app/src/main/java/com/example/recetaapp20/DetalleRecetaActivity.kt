package com.example.recetaapp20

import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.FirebaseFirestore

class DetalleRecetaActivity : AppCompatActivity() {

    private lateinit var db: FirebaseFirestore
    private lateinit var btnCancelar: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalle_receta)

        val recetaNombre = findViewById<TextView>(R.id.recetaNombre)
        val recetaTiempo = findViewById<TextView>(R.id.recetaTiempo)
        val recetaDificultad = findViewById<TextView>(R.id.recetaDificultad)
        val recetaInstrucciones = findViewById<TextView>(R.id.recetaInstrucciones)
        val recetaIngredientes = findViewById<TextView>(R.id.recetaIngredientes)

        db = FirebaseFirestore.getInstance()
        btnCancelar = findViewById(R.id.btnBackToLogin)

        // Extras
        val username = intent.getStringExtra("USERNAME")
        val user_id = intent.getStringExtra("USER_ID")

        btnCancelar.setOnClickListener {
            intent.putExtra("USERNAME", username)
            intent.putExtra("USER_ID", user_id)
            finish()  // Termina la actividad y vuelve a la actividad anterior
        }

        val recetaId = intent.getStringExtra("RECETA_ID")

        if (recetaId != null) {
            db.collection("recetas").document(recetaId).get()
                .addOnSuccessListener { document ->
                    val receta = document.toObject(Receta::class.java)
                    if (receta != null) {
                        recetaNombre.text = receta.nombre
                        recetaTiempo.text = "Tiempo: ${receta.tiempoMins} minutos"
                        recetaDificultad.text = "Dificultad: ${receta.dificultad}"
                        recetaInstrucciones.text = "Instrucciones: \n${receta.instrucciones.joinToString("\n")}"

                        // Si los ingredientes son IDs de documentos, necesitamos obtener los nombres
                        val ingredientes = receta.ingredientes // Asumiendo que es una lista de IDs

                        // Cargar los nombres de los ingredientes
                        val ingredientesNombres = mutableListOf<String>()
                        for (idIngrediente in ingredientes) {
                            db.collection("ingredientes").document(idIngrediente).get()
                                .addOnSuccessListener { ingredienteDoc ->
                                    val nombreIngrediente = ingredienteDoc.getString("nombre")
                                    if (nombreIngrediente != null) {
                                        ingredientesNombres.add(nombreIngrediente)
                                    }

                                    // Cuando todos los ingredientes han sido cargados, los mostramos
                                    if (ingredientesNombres.size == ingredientes.size) {
                                        recetaIngredientes.text = "Ingredientes: \n${ingredientesNombres.joinToString("\n")}"
                                    }
                                }
                        }

                    }
                }
                .addOnFailureListener {
                    recetaNombre.text = "Error al cargar los detalles"
                }
        }
    }
}

