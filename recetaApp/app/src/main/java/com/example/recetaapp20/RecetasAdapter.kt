package com.example.recetaapp20

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.FirebaseFirestore
import java.util.Locale

class RecetasAdapter(
    private val recetas: List<Receta>,
    private val userId: String,
    private val onItemClick: (Receta) -> Unit,
    private val onDeleteClick: ((Receta) -> Unit)? = null, // Hacemos que el parámetro de eliminar sea opcional
    private val esExplorar: Boolean = false, // Parámetro para controlar el layout sea opcional
    private val showFavoritos: Boolean = false // Filtro para ver (o no) los favoritos
) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), Filterable {

    private var recetasFiltradas: List<Receta> = recetas
    private val favoritos = mutableSetOf<String>()

    companion object {
        const val ITEM_TYPE_NORMAL = 0
        const val ITEM_TYPE_EXPLORAR = 1
    }

    override fun getItemViewType(position: Int): Int {
        return if (esExplorar) {
            ITEM_TYPE_EXPLORAR
        } else {
            ITEM_TYPE_NORMAL
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            ITEM_TYPE_EXPLORAR -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_receta_explorar, parent, false)
                RecetaExplorarViewHolder(view)
            }
            ITEM_TYPE_NORMAL -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_receta, parent, false)
                RecetaNormalViewHolder(view)
            }
            else -> throw IllegalArgumentException("Invalid view type")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val receta = recetasFiltradas[position]
        when (holder) {
            is RecetaNormalViewHolder -> holder.bind(receta, favoritos.contains(receta.id), onItemClick, onDeleteClick)
            is RecetaExplorarViewHolder -> holder.bind(receta, favoritos.contains(receta.id), onItemClick, onDeleteClick) { toggleFavorito ->
                if (toggleFavorito) {
                    // Agregar a favoritos
                    favoritos.add(receta.id)
                    agregarAFavoritos(receta.id)
                } else {
                    // Eliminar de favoritos
                    favoritos.remove(receta.id)
                    eliminarDeFavoritos(receta.id)
                }

                // Refrescar solo el ítem que fue modificado (la estrella)
                notifyItemChanged(position)

                // Actualizar el filtro después de agregar o eliminar un favorito
                filter.filter("") // Esto vuelve a aplicar el filtro y actualiza la lista
            }
        }
    }

    override fun getItemCount() = recetasFiltradas.size

    private fun agregarAFavoritos(recetaId: String) {
        val favorita = Favorita("", userId, recetaId)

        // Agregar a Firestore
        FirebaseFirestore.getInstance()
            .collection("favoritas")
            .add(favorita)
            .addOnSuccessListener { documentReference ->
                val idGenerado = documentReference.id
                FirebaseFirestore.getInstance()
                    .collection("favoritas")
                    .document(idGenerado)
                    .update("id", idGenerado)
                    .addOnSuccessListener {
                        Log.e("Favorita", "Favorita añadida: $idGenerado")
                    }
                    .addOnFailureListener { e ->
                        Log.e("Favorita", "Error al actualizar el campo 'id': ", e)
                    }
            }
            .addOnFailureListener { e ->
                Log.e("Favorita", "Error al agregar favorita: ", e)
            }
    }

    private fun eliminarDeFavoritos(recetaId: String) {
        FirebaseFirestore.getInstance()
            .collection("favoritas")
            .whereEqualTo("id_usuario", userId)
            .whereEqualTo("id_receta", recetaId)
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    FirebaseFirestore.getInstance().collection("favoritas").document(document.id).delete()
                }
            }
    }

    class RecetaNormalViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val recetaNombre: TextView = itemView.findViewById(R.id.recetaNombre)
        private val recetaDificultad: TextView = itemView.findViewById(R.id.recetaDificultad)
        private val btnEliminar: Button = itemView.findViewById(R.id.btnEliminarReceta)

        fun bind(
            receta: Receta,
            esFavorito: Boolean,
            onItemClick: (Receta) -> Unit,
            onDeleteClick: ((Receta) -> Unit)?
        ) {
            recetaNombre.text = receta.nombre
            recetaDificultad.text = "Dificultad: ${receta.dificultad}"

            itemView.setOnClickListener { onItemClick(receta) }

            btnEliminar.visibility = if (onDeleteClick != null) View.VISIBLE else View.GONE
            btnEliminar.setOnClickListener { onDeleteClick?.invoke(receta) }
        }
    }

    class RecetaExplorarViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val recetaNombre: TextView = itemView.findViewById(R.id.recetaNombre)
        private val recetaDificultad: TextView = itemView.findViewById(R.id.recetaDificultad)
        private val recetaUsuario: TextView = itemView.findViewById(R.id.recetaUsuario)
        private val btnEliminar: Button = itemView.findViewById(R.id.btnEliminarReceta)
        private val btnFavorito: ImageButton = itemView.findViewById(R.id.btnFavorito)

        fun bind(
            receta: Receta,
            esFavorito: Boolean,
            onItemClick: (Receta) -> Unit,
            onDeleteClick: ((Receta) -> Unit)?,
            onFavoritoClick: (Boolean) -> Unit
        ) {
            recetaNombre.text = receta.nombre
            recetaDificultad.text = "Dificultad: ${receta.dificultad}"
            recetaUsuario.text = "Creado por: ${receta.usuario}"

            itemView.setOnClickListener { onItemClick(receta) }

            btnEliminar.visibility = if (onDeleteClick != null) View.VISIBLE else View.GONE
            btnEliminar.setOnClickListener { onDeleteClick?.invoke(receta) }

            // Actualiza el ícono de la estrella según si es favorito o no
            btnFavorito.setImageResource(if (esFavorito) R.drawable.ic_star_filled else R.drawable.ic_star_border)

            btnFavorito.setOnClickListener {
                // Alterna el estado del favorito
                onFavoritoClick(!esFavorito)

                // Cambia el ícono de la estrella
                btnFavorito.setImageResource(if (!esFavorito) R.drawable.ic_star_filled else R.drawable.ic_star_border)
            }
        }
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val filteredList = ArrayList<Receta>()

                if (constraint == null || constraint.isEmpty()) {
                    filteredList.addAll(recetas)
                } else {
                    val filterPattern = constraint.toString().lowercase(Locale.getDefault()).trim()

                    for (receta in recetas) {
                        if (receta.nombre.lowercase(Locale.getDefault()).contains(filterPattern)) {
                            filteredList.add(receta)
                        }
                    }
                }

                // Filtramos para mostrar solo los favoritos si es necesario
                if (showFavoritos) {
                    filteredList.retainAll { favoritos.contains(it.id) }
                }

                val results = FilterResults()
                results.values = filteredList
                return results
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                recetasFiltradas = results?.values as List<Receta>
                notifyDataSetChanged()
            }
        }
    }

    // Método que actualiza el filtro de favoritos
    fun updateFavoritosFilter(favoritos: Set<String>) {
        this.favoritos.clear()
        this.favoritos.addAll(favoritos)
        filter.filter("") // Realizamos el filtro
    }
}



