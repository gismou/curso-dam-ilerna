package com.example.recetaapp20

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.FirebaseFirestore

class EditarRecetaActivity : AppCompatActivity() {

    private lateinit var db: FirebaseFirestore
    private lateinit var nombreEditText: EditText
    private lateinit var dificultadSpinner: Spinner
    private lateinit var tiempoMinsEditText: EditText
    private lateinit var instruccionesEditText: EditText
    private lateinit var btnGuardar: Button
    private lateinit var btnCancelar: Button
    private lateinit var spinnerIngredientes: Spinner
    private lateinit var btnAgregarIngrediente: Button
    private lateinit var ingredientesLayout: LinearLayout
    private val ingredientesList = mutableListOf<Ingrediente>()
    private val ingredientesSeleccionados = mutableListOf<String>() // IDs de los ingredientes seleccionados
    private lateinit var ingredientesAdapter: ArrayAdapter<Ingrediente>
    private var recetaId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_editar_receta)

        // Inicializar Firebase y referencias
        db = FirebaseFirestore.getInstance()

        // Inicializar vistas
        spinnerIngredientes = findViewById(R.id.spinnerIngredientes)

        // Extras recibidos
        val username = intent.getStringExtra("USERNAME")
        val user_id = intent.getStringExtra("USER_ID")

        // Inicializar los campos de la UI
        nombreEditText = findViewById(R.id.nombreReceta)
        dificultadSpinner = findViewById(R.id.dificultadSpinner)
        tiempoMinsEditText = findViewById(R.id.tiempoMins)
        instruccionesEditText = findViewById(R.id.instrucciones)
        btnGuardar = findViewById(R.id.btnGuardarReceta)
        btnCancelar = findViewById(R.id.btnBackToLogin)
        spinnerIngredientes = findViewById(R.id.spinnerIngredientes)
        btnAgregarIngrediente = findViewById(R.id.btnAgregarIngrediente)
        ingredientesLayout = findViewById(R.id.ingredientesLayout)

        // Obtener el ID de la receta para editar
        recetaId = intent.getStringExtra("RECETA_ID")

        // Configurar Spinner de dificultad
        val dificultadOptions = arrayOf("Fácil", "Media", "Difícil")
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, dificultadOptions)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        dificultadSpinner.adapter = adapter

        // Configurar adaptador del Spinner ingredientes
        ingredientesAdapter = ArrayAdapter(
            this,
            android.R.layout.simple_spinner_item,
            ingredientesList
        )

        // Vista desplegable para el Spinner ingredientes
        ingredientesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerIngredientes.adapter = ingredientesAdapter

        // Cargar ingredientes del usuario actual
        if (user_id != null) {
            cargarIngredientesUsuario(user_id)
        }

        // Listener para agregar ingrediente
        btnAgregarIngrediente.setOnClickListener {
            val ingredienteSeleccionado = spinnerIngredientes.selectedItem as Ingrediente
            if (!ingredientesSeleccionados.contains(ingredienteSeleccionado.id)) {
                ingredientesSeleccionados.add(ingredienteSeleccionado.id)
                mostrarIngredientesSeleccionados()
            } else {
                Toast.makeText(this, "Este ingrediente ya ha sido agregado", Toast.LENGTH_SHORT).show()
            }
        }

        // Cargar receta existente si hay ID
        if (recetaId != null) {
            cargarRecetaParaEditar(recetaId!!)
        }

        // Guardar cambios
        btnGuardar.setOnClickListener {
            if (user_id != null) {
                guardarReceta(user_id)
            }
        }

        // Cancelar y volver
        btnCancelar.setOnClickListener {
            intent.putExtra("USERNAME", username)
            intent.putExtra("USER_ID", user_id)
            finish() // Volver a la actividad anterior
        }
    }

    // Guardar o actualizar receta
    private fun guardarReceta(user_id: String) {
        val nombre = nombreEditText.text.toString().trim()
        val dificultad = dificultadSpinner.selectedItem.toString()
        val tiempoMins = tiempoMinsEditText.text.toString().toIntOrNull() ?: 0
        val instrucciones = instruccionesEditText.text.toString().split("\n").map { it.trim() }

        if (nombre.isEmpty() || dificultad.isEmpty() || instrucciones.isEmpty()) {
            Toast.makeText(this, "Todos los campos son obligatorios", Toast.LENGTH_SHORT).show()
            return
        }

        // Verificar si ya existe una receta con el mismo nombre para el mismo usuario
        db.collection("recetas")
            .whereEqualTo("usuario", user_id)
            .whereEqualTo("nombre", nombre)
            .get()
            .addOnSuccessListener { documents ->
                val recetaExistente = documents.documents.firstOrNull()

                // Si estamos editando y el ID coincide con el actual, permitimos la edición
                if (recetaId != null && recetaExistente?.id == recetaId) {
                    guardarOActualizarReceta(nombre, dificultad, tiempoMins, instrucciones, user_id)
                    return@addOnSuccessListener
                }

                // Si existe otra receta con el mismo nombre, mostrar error
                if (recetaExistente != null) {
                    Toast.makeText(this, "Ya existe una receta con este nombre para este usuario", Toast.LENGTH_SHORT).show()
                    return@addOnSuccessListener
                }

                // Si no existe ninguna receta con el mismo nombre, proceder a guardar o actualizar
                guardarOActualizarReceta(nombre, dificultad, tiempoMins, instrucciones, user_id)
            }
            .addOnFailureListener { e ->
                Toast.makeText(this, "Error al verificar la receta: ${e.message}", Toast.LENGTH_SHORT).show()
            }
    }

    private fun guardarOActualizarReceta(
        nombre: String,
        dificultad: String,
        tiempoMins: Int,
        instrucciones: List<String>,
        userId: String
    ) {
        val receta = Receta(
            nombre = nombre,
            tiempoMins = tiempoMins,
            usuario = userId,
            dificultad = dificultad,
            instrucciones = instrucciones,
            ingredientes = ingredientesSeleccionados,
            id = recetaId ?: ""
        )

        if (recetaId != null) {
            // Actualizar receta existente
            db.collection("recetas").document(recetaId!!)
                .set(receta)
                .addOnSuccessListener {
                    Toast.makeText(this, "Receta actualizada con éxito", Toast.LENGTH_SHORT).show()
                    setResult(RESULT_OK, Intent().putExtra("RECETA_GUARDADA", true))
                    finish()
                }
                .addOnFailureListener { e ->
                    Toast.makeText(this, "Error al actualizar la receta: ${e.message}", Toast.LENGTH_SHORT).show()
                }
        } else {
            // Crear una nueva receta
            db.collection("recetas").add(receta)
                .addOnSuccessListener {
                    Toast.makeText(this, "Receta guardada con éxito", Toast.LENGTH_SHORT).show()
                    setResult(RESULT_OK, Intent().putExtra("RECETA_GUARDADA", true))
                    finish()
                }
                .addOnFailureListener { e ->
                    Toast.makeText(this, "Error al guardar la receta: ${e.message}", Toast.LENGTH_SHORT).show()
                }
        }
    }

    // Cargar los ingredientes para mostrar en el spinner
    private fun cargarIngredientesUsuario(user_id: String) {
        db.collection("ingredientes")
            .whereEqualTo("usuario", user_id)
            .get()
            .addOnSuccessListener { documents ->
                ingredientesList.clear()
                for (document in documents) {
                    val ingrediente = document.toObject(Ingrediente::class.java).copy(id = document.id)
                    ingredientesList.add(ingrediente)
                }
                ingredientesAdapter.notifyDataSetChanged()
            }
            .addOnFailureListener { e ->
                Toast.makeText(this, "Error al cargar ingredientes: ${e.message}", Toast.LENGTH_SHORT).show()
            }
    }

    private fun mostrarIngredientesSeleccionados() {
        // Mostrar los ingredientes seleccionados en el layout
        ingredientesLayout.removeAllViews()
        ingredientesSeleccionados.forEach { ingredienteId ->
            val ingrediente = ingredientesList.find { it.id == ingredienteId }
            val ingredienteView = layoutInflater.inflate(R.layout.item_ingrediente_seleccionado, null)

            val textIngrediente = ingredienteView.findViewById<TextView>(R.id.textIngredienteSeleccionado)
            textIngrediente.text = ingrediente?.nombre

            val btnQuitarIngrediente = ingredienteView.findViewById<Button>(R.id.btnQuitarIngrediente)
            btnQuitarIngrediente.setOnClickListener {
                ingredientesSeleccionados.remove(ingredienteId)
                mostrarIngredientesSeleccionados() // Actualizar la vista
            }

            ingredientesLayout.addView(ingredienteView)
        }
    }

    private fun cargarRecetaParaEditar(id: String) {
        db.collection("recetas").document(id)
            .get()
            .addOnSuccessListener { document ->
                if (document.exists()) {
                    val receta = document.toObject(Receta::class.java)
                    receta?.let {
                        nombreEditText.setText(it.nombre)
                        dificultadSpinner.setSelection(getIndexOfDificultad(it.dificultad))
                        tiempoMinsEditText.setText(it.tiempoMins.toString())
                        instruccionesEditText.setText(it.instrucciones.joinToString("\n"))
                        ingredientesSeleccionados.addAll(it.ingredientes)
                        mostrarIngredientesSeleccionados()
                    }
                }
            }
            .addOnFailureListener { e ->
                Toast.makeText(this, "Error al cargar receta: ${e.message}", Toast.LENGTH_SHORT).show()
            }
    }

    private fun getIndexOfDificultad(dificultad: String): Int {
        return when (dificultad) {
            "Fácil" -> 0
            "Media" -> 1
            "Difícil" -> 2
            else -> 0
        }
    }
}
