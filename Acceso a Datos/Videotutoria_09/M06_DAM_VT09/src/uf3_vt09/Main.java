package uf3_vt09;


import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

public class Main {

	public static void main(String[] args) {
		try {
			// Creamos la estructura para trabajar con el XML
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true);
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse("src/libro.xml");
			
			// Creamos el XPath
			XPathFactory xpathFactory = XPathFactory.newInstance();
			XPath xpath = xpathFactory.newXPath();
			
			// Realizamos la consulta XPath
			System.out.println("\n1) Recogemos los títulos de los libros con precio mayor de 20 euros");
			XPathExpression expr = xpath.compile("//libro[precio>20]/titulo/text()");
			Object result = expr.evaluate(doc, XPathConstants.NODESET);
			NodeList nodes = (NodeList) result;
			for(int i=0; i<nodes.getLength(); i++) {
				System.out.println(nodes.item(i).getNodeValue());
			}	
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

}
